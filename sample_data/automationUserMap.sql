--TL Module Users

--To Expire PHS JA/SA
Update egeis_assignment set todate=current_date-1 where department = (select id from eg_department where lower(name) = lower('PUBLIC HEALTH AND SANITATION')) and designation = (select id from eg_designation where lower(name) = lower('Junior Assistant'));

Update egeis_assignment set todate=current_date-1 where department = (select id from eg_department where lower(name) = lower('PUBLIC HEALTH AND SANITATION')) and designation = (select id from eg_designation where lower(name) = lower('Senior Assistant'));

--To Expire REV JA/SA assignment

Update egeis_assignment set todate=current_date-1 where department in (select id from eg_department where name = 'REVENUE') and designation in (select id from eg_designation where name = 'Junior Assistant');

Update egeis_assignment set todate=current_date-1 where department in (select id from eg_department where name = 'REVENUE') and designation in (select id from eg_designation where name = 'Senior Assistant');

--To expire the PHS BDR assignment

Update egeis_assignment set todate=current_date-1 where department = (select id from eg_department where lower(name) = lower('PUBLIC HEALTH AND SANITATION')) and designation = (select id from eg_designation where lower(name) = lower('Health Assistant/Birth and Death Registrar'));


--To expire ADM JA and SA assignment

Update egeis_assignment set todate=current_date-1 where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Junior Assistant'));

Update egeis_assignment set todate=current_date-1 where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Senior Assistant'));

--To expire the current Manager assignment

Update egeis_assignment set todate=current_date-1 where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Manager'));


--Correcting sequence number
select setval('seq_eg_position', (select max(id)+1 from eg_position));

--Create PHS JA
insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Junior Assistant')),(select id from eg_department where lower(name)=lower('PUBLIC HEALTH AND SANITATION')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'TL_PHS_JA',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('PUBLIC HEALTH AND SANITATION')) and designation = (select id from eg_designation where lower(name) = lower('Junior Assistant'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'TL_PHS_JA');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'TLJAOne', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'TLJuniorAssistantOne',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='TLJAOne'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='TLJAOne') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='TLJAOne'), id from eg_role where name = 'TLApprover' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='TLJAOne') and roleid=(select id from eg_role where name = 'TLApprover'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='TLJAOne'), id from eg_role where name = 'Collection Operator' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='TLJAOne') and roleid=(select id from eg_role where name = 'Collection Operator'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='TLJAOne'), id from eg_role where name = 'TLCreator' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='TLJAOne') and roleid=(select id from eg_role where name = 'TLCreator'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='TLJAOne'), id from eg_role where name = 'TL VIEW ACCESS' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='TLJAOne') and roleid=(select id from eg_role where name = 'TL VIEW ACCESS'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='TLJAOne'),'TLJAOne','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Junior Assistant')),null, (select id from eg_department where lower(name) = lower('PUBLIC HEALTH AND SANITATION')),(select id from eg_position where name='TL_PHS_JA'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='TLJAOne'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('PUBLIC HEALTH AND SANITATION')) and designation = (select id from eg_designation where lower(name) = lower('Junior Assistant')) and position = (select id from eg_position where name='TL_PHS_JA') and employee = (select id from egeis_employee where code='TLJAOne'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='TLJAOne'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='TLJAOne') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));
 
--Create PHS SI
insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Sanitary Inspector')),(select id from eg_department where lower(name)=lower('PUBLIC HEALTH AND SANITATION')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'TL_PHS_SI',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('PUBLIC HEALTH AND SANITATION')) and designation = (select id from eg_designation where lower(name) = lower('Sanitary Inspector'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'TL_PHS_SI');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'TLSIOne', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'TLSanitaryInspectorOne',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='TLSIOne'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='TLSIOne') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='TLSIOne'), id from eg_role where name = 'TLApprover' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='TLSIOne') and roleid=(select id from eg_role where name = 'TLApprover'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='TLSIOne'), id from eg_role where name = 'TL VIEW ACCESS' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='TLSIOne') and roleid=(select id from eg_role where name = 'TL VIEW ACCESS'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='TLSIOne'),'TLSIOne','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Sanitary Inspector')),null, (select id from eg_department where lower(name) = lower('PUBLIC HEALTH AND SANITATION')),(select id from eg_position where name='TL_PHS_SI'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='TLSIOne'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('PUBLIC HEALTH AND SANITATION')) and designation = (select id from eg_designation where lower(name) = lower('Sanitary Inspector')) and position = (select id from eg_position where name='TL_PHS_SI') and employee = (select id from egeis_employee where code='TLSIOne'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='TLSIOne'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='TLSIOne') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));

--Create PHS SS

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Sanitary Supervisor')),(select id from eg_department where lower(name)=lower('PUBLIC HEALTH AND SANITATION')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'TL_PHS_SS',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('PUBLIC HEALTH AND SANITATION')) and designation = (select id from eg_designation where lower(name) = lower('Sanitary Supervisor'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'TL_PHS_SS');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'TLSSOne', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'TLSanitarySupervisorOne',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='TLSSOne'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='TLSSOne') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='TLSSOne'), id from eg_role where name = 'TLApprover' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='TLSSOne') and roleid=(select id from eg_role where name = 'TLApprover'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='TLSSOne'), id from eg_role where name = 'TL VIEW ACCESS' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='TLSSOne') and roleid=(select id from eg_role where name = 'TL VIEW ACCESS'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='TLSSOne'),'TLSSOne','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Sanitary Supervisor')),null, (select id from eg_department where lower(name) = lower('PUBLIC HEALTH AND SANITATION')),(select id from eg_position where name='TL_PHS_SS'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='TLSSOne'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('PUBLIC HEALTH AND SANITATION')) and designation = (select id from eg_designation where lower(name) = lower('Sanitary Supervisor')) and position = (select id from eg_position where name='TL_PHS_SS') and employee = (select id from egeis_employee where code='TLSSOne'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='TLSSOne'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='TLSSOne') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));

--Create PHS AMOH

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(code)=lower('AMOH')),(select id from eg_department where lower(name)=lower('PUBLIC HEALTH AND SANITATION')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'TL_PHS_AMOH',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('PUBLIC HEALTH AND SANITATION')) and designation = (select id from eg_designation where lower(code) = lower('AMOH'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'TL_PHS_AMOH');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'TLAMOHOne', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'TLAMOHOne',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='TLAMOHOne'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='TLAMOHOne') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='TLAMOHOne'), id from eg_role where name = 'TLApprover' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='TLAMOHOne') and roleid=(select id from eg_role where name = 'TLApprover'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='TLAMOHOne'), id from eg_role where name = 'TL VIEW ACCESS' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='TLAMOHOne') and roleid=(select id from eg_role where name = 'TL VIEW ACCESS'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='TLAMOHOne'),'TLAMOHOne','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(code) = lower('AMOH')),null, (select id from eg_department where lower(name) = lower('PUBLIC HEALTH AND SANITATION')),(select id from eg_position where name='TL_PHS_AMOH'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='TLAMOHOne'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('PUBLIC HEALTH AND SANITATION')) and designation = (select id from eg_designation where lower(code) = lower('AMOH')) and position = (select id from eg_position where name='TL_PHS_AMOH') and employee = (select id from egeis_employee where code='TLAMOHOne'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='TLAMOHOne'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='TLAMOHOne') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));

--Create PHS MHO

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Municipal Health Officer')),(select id from eg_department where lower(name)=lower('PUBLIC HEALTH AND SANITATION')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'TL_PHS_MHO',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('PUBLIC HEALTH AND SANITATION')) and designation = (select id from eg_designation where lower(name) = lower('Municipal Health Officer'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'TL_PHS_MHO');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'TLMHOOne', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'TLMHOOne',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='TLMHOOne'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='TLMHOOne') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='TLMHOOne'), id from eg_role where name = 'TLApprover' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='TLMHOOne') and roleid=(select id from eg_role where name = 'TLApprover'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='TLMHOOne'), id from eg_role where name = 'TL VIEW ACCESS' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='TLMHOOne') and roleid=(select id from eg_role where name = 'TL VIEW ACCESS'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='TLMHOOne'),'TLMHOOne','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Municipal Health Officer')),null, (select id from eg_department where lower(name) = lower('PUBLIC HEALTH AND SANITATION')),(select id from eg_position where name='TL_PHS_MHO'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='TLMHOOne'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('PUBLIC HEALTH AND SANITATION')) and designation = (select id from eg_designation where lower(name) = lower('Municipal Health Officer')) and position = (select id from eg_position where name='TL_PHS_MHO') and employee = (select id from egeis_employee where code='TLMHOOne'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='TLMHOOne'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='TLMHOOne') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));

--Create PHS CMOH */

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Chief Medical Officer of Health')),(select id from eg_department where lower(name)=lower('PUBLIC HEALTH AND SANITATION')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'TL_PHS_CMOH',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('PUBLIC HEALTH AND SANITATION')) and designation = (select id from eg_designation where lower(name) = lower('Chief Medical Officer of Health'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'TL_PHS_CMOH');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'TLCMOHOne', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'TLCMOHOne',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='TLCMOHOne'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='TLCMOHOne') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='TLCMOHOne'), id from eg_role where name = 'TLApprover' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='TLCMOHOne') and roleid=(select id from eg_role where name = 'TLApprover'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='TLCMOHOne'), id from eg_role where name = 'TL VIEW ACCESS' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='TLCMOHOne') and roleid=(select id from eg_role where name = 'TL VIEW ACCESS'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='TLCMOHOne'),'TLCMOHOne','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Chief Medical Officer of Health')),null, (select id from eg_department where lower(name) = lower('PUBLIC HEALTH AND SANITATION')),(select id from eg_position where name='TL_PHS_CMOH'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='TLCMOHOne'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('PUBLIC HEALTH AND SANITATION')) and designation = (select id from eg_designation where lower(name) = lower('Chief Medical Officer of Health')) and position = (select id from eg_position where name='TL_PHS_CMOH') and employee = (select id from egeis_employee where code='TLCMOHOne'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='TLCMOHOne'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='TLCMOHOne') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));

--Create ADM Commissioner

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Commissioner')),(select id from eg_department where lower(name)=lower('ADMINISTRATION')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'TL_ADM_Commissioner',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Commissioner'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'TL_ADM_Commissioner');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'TLCommissionerOne', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'TLCommissionerOne',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='TLCommissionerOne'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='TLCommissionerOne') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='TLCommissionerOne'), id from eg_role where name = 'TLApprover' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='TLCommissionerOne') and roleid=(select id from eg_role where name = 'TLApprover'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='TLCommissionerOne'), id from eg_role where name = 'TL VIEW ACCESS' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='TLCommissionerOne') and roleid=(select id from eg_role where name = 'TL VIEW ACCESS'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='TLCommissionerOne'),'TLCommissionerOne','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Commissioner')),null, (select id from eg_department where lower(name) = lower('ADMINISTRATION')),(select id from eg_position where name='TL_ADM_Commissioner'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='TLCommissionerOne'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Commissioner')) and position = (select id from eg_position where name='TL_ADM_Commissioner') and employee = (select id from egeis_employee where code='TLCommissionerOne'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='TLCommissionerOne'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='TLCommissionerOne') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));

--LAMS Module Users

--Create REV JA

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Junior Assistant')),(select id from eg_department where lower(name)=lower('REVENUE')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'LAMS_REV_JA_02',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('REVENUE')) and designation = (select id from eg_designation where lower(name) = lower('Junior Assistant'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'LAMS_REV_JA_02');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'LAMSJATWO', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'LAMSJuniorAssistantTwo',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='LAMSJATWO'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='LAMSJATWO') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='LAMSJATWO'), id from eg_role where name = 'ULB Operator' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='LAMSJATWO') and roleid=(select id from eg_role where name = 'ULB Operator'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='LAMSJATWO'), id from eg_role where name = 'Collection Operator' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='LAMSJATWO') and roleid=(select id from eg_role where name = 'Collection Operator'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='LAMSJATWO'),'LAMSJATWO','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Junior Assistant')),null, (select id from eg_department where lower(name) = lower('REVENUE')),(select id from eg_position where name='LAMS_REV_JA_02'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='LAMSJATWO'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('REVENUE')) and designation = (select id from eg_designation where lower(name) = lower('Junior Assistant')) and position = (select id from eg_position where name='LAMS_REV_JA_02') and employee = (select id from egeis_employee where code='LAMSJATWO'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='LAMSJATWO'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='LAMSJATWO') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));

--Create REV RO

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Revenue Officer')),(select id from eg_department where lower(name)=lower('REVENUE')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'LAMS_REV_RO_02',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('REVENUE')) and designation = (select id from eg_designation where lower(name) = lower('Revenue Officer'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'LAMS_REV_RO_02');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'LAMSROTWO', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'LAMSRevenueOfficerTwo',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='LAMSROTWO'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='LAMSROTWO') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='LAMSROTWO'), id from eg_role where name = 'Property Verifier' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='LAMSROTWO') and roleid=(select id from eg_role where name = 'Property Verifier'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='LAMSROTWO'), id from eg_role where name = 'Property Approver' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='LAMSROTWO') and roleid=(select id from eg_role where name = 'Property Approver'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='LAMSROTWO'),'LAMSROTWO','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Revenue Officer')),null, (select id from eg_department where lower(name) = lower('REVENUE')),(select id from eg_position where name='LAMS_REV_RO_02'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='LAMSROTWO'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('REVENUE')) and designation = (select id from eg_designation where lower(name) = lower('Revenue Officer')) and position = (select id from eg_position where name='LAMS_REV_RO_02') and employee = (select id from egeis_employee where code='LAMSROTWO'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='LAMSROTWO'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='LAMSROTWO') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));

-- Create ADM Assistant Commissioner

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Assistant Commissioner')),(select id from eg_department where lower(name)=lower('ADMINISTRATION')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'LAMS_ADM_ACOMM_02',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Assistant Commissioner'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'LAMS_ADM_ACOMM_02');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'LAMSACTWO', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'LAMSAssistantCommissionerTwo',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='LAMSACTWO'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='LAMSACTWO') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='LAMSACTWO'), id from eg_role where name = 'Property Verifier' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='LAMSACTWO') and roleid=(select id from eg_role where name = 'Property Verifier'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='LAMSACTWO'), id from eg_role where name = 'Property Approver' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='LAMSACTWO') and roleid=(select id from eg_role where name = 'Property Approver'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='LAMSACTWO'),'LAMSACTWO','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Assistant Commissioner')),null, (select id from eg_department where lower(name) = lower('ADMINISTRATION')),(select id from eg_position where name='LAMS_ADM_ACOMM_02'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='LAMSACTWO'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Assistant Commissioner')) and position = (select id from eg_position where name='LAMS_ADM_ACOMM_02') and employee = (select id from egeis_employee where code='LAMSACTWO'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='LAMSACTWO'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='LAMSACTWO') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));

--Create ADM Deputy Commissioner

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Deputy Commissioner')),(select id from eg_department where lower(name)=lower('ADMINISTRATION')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'LAMS_ADM_DCOMM_02',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Deputy Commissioner'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'LAMS_ADM_DCOMM_02');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'LAMSDCTWO', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'LAMSDeputyCommissionerTwo',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='LAMSDCTWO'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='LAMSDCTWO') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='LAMSDCTWO'), id from eg_role where name = 'Property Verifier' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='LAMSDCTWO') and roleid=(select id from eg_role where name = 'Property Verifier'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='LAMSDCTWO'), id from eg_role where name = 'Property Approver' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='LAMSDCTWO') and roleid=(select id from eg_role where name = 'Property Approver'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='LAMSDCTWO'),'LAMSDCTWO','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Deputy Commissioner')),null, (select id from eg_department where lower(name) = lower('ADMINISTRATION')),(select id from eg_position where name='LAMS_ADM_DCOMM_02'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='LAMSDCTWO'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Deputy Commissioner')) and position = (select id from eg_position where name='LAMS_ADM_DCOMM_02') and employee = (select id from egeis_employee where code='LAMSDCTWO'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='LAMSDCTWO'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='LAMSDCTWO') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));

-- Create ADM Commissioner

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Commissioner')),(select id from eg_department where lower(name)=lower('ADMINISTRATION')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'LAMS_ADM_COMM_02',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Commissioner'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'LAMS_ADM_COMM_02');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'LAMSCOMMTWO', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'LAMSCommissionerTwo',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='LAMSCOMMTWO'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='LAMSCOMMTWO') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='LAMSCOMMTWO'), id from eg_role where name = 'Property Approver' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='LAMSCOMMTWO') and roleid=(select id from eg_role where name = 'Property Approver'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='LAMSCOMMTWO'),'LAMSCOMMTWO','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Commissioner')),null, (select id from eg_department where lower(name) = lower('ADMINISTRATION')),(select id from eg_position where name='LAMS_ADM_COMM_02'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='LAMSCOMMTWO'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Commissioner')) and position = (select id from eg_position where name='LAMS_ADM_COMM_02') and employee = (select id from egeis_employee where code='LAMSCOMMTWO'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='LAMSCOMMTWO'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='LAMSCOMMTWO') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));

--Asset Users

--Create REV JA

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Junior Assistant')),(select id from eg_department where lower(name)=lower('REVENUE')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'ASSETS_JA_02',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('REVENUE')) and designation = (select id from eg_designation where lower(name) = lower('Junior Assistant'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'ASSETS_JA_02');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'ASSETSJATWO', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'ASSETSJuniorAssistantTwo',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ASSETSJATWO'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ASSETSJATWO') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ASSETSJATWO'), id from eg_role where name = 'Asset Administrator' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ASSETSJATWO') and roleid=(select id from eg_role where name = 'Asset Administrator'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='ASSETSJATWO'),'ASSETSJATWO','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Junior Assistant')),null, (select id from eg_department where lower(name) = lower('REVENUE')),(select id from eg_position where name='ASSETS_JA_02'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='ASSETSJATWO'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('REVENUE')) and designation = (select id from eg_designation where lower(name) = lower('Junior Assistant')) and position = (select id from eg_position where name='ASSETS_JA_02') and employee = (select id from egeis_employee where code='ASSETSJATWO'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='ASSETSJATWO'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='ASSETSJATWO') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));

--EIS Users

--Create REV AO

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Accounts Officer')),(select id from eg_department where lower(name)=lower('REVENUE')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'EIS_REV_ACOF',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('REVENUE')) and designation = (select id from eg_designation where lower(name) = lower('Accounts Officer'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'EIS_REV_ACOF');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'EISACOFOne', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'EISAccountOfficerOne',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='EISACOFOne'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='EISACOFOne') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='EISACOFOne'), id from eg_role where name = 'Employee Admin' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='EISACOFOne') and roleid=(select id from eg_role where name = 'Employee Admin'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='EISACOFOne'), id from eg_role where name = 'EIS_VIEW_ACCESS' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='EISACOFOne') and roleid=(select id from eg_role where name = 'EIS_VIEW_ACCESS'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='EISACOFOne'), id from eg_role where name = 'EIS Report Viewer' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='EISACOFOne') and roleid=(select id from eg_role where name = 'EIS Report Viewer'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='EISACOFOne'),'EISACOFOne','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Accounts Officer')),null, (select id from eg_department where lower(name) = lower('REVENUE')),(select id from eg_position where name='EIS_REV_ACOF'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='EISACOFOne'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('REVENUE')) and designation = (select id from eg_designation where lower(name) = lower('Accounts Officer')) and position = (select id from eg_position where name='EIS_REV_ACOF') and employee = (select id from egeis_employee where code='EISACOFOne'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='EISACOFOne'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='EISACOFOne') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));

--Create ADM Commissioner

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Commissioner')),(select id from eg_department where lower(name)=lower('ADMINISTRATION')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'EIS_ADM_Commissioner',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Commissioner'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'EIS_ADM_Commissioner');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'EISCommissionerOne', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'EISCommissionerOne',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='EISCommissionerOne'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='EISCommissionerOne') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='EISCommissionerOne'), id from eg_role where name = 'Employee Admin' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='EISCommissionerOne') and roleid=(select id from eg_role where name = 'Employee Admin'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='EISCommissionerOne'), id from eg_role where name = 'EIS_VIEW_ACCESS' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='EISCommissionerOne') and roleid=(select id from eg_role where name = 'EIS_VIEW_ACCESS'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='EISCommissionerOne'), id from eg_role where name = 'EIS Report Viewer' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='EISCommissionerOne') and roleid=(select id from eg_role where name = 'EIS Report Viewer'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='EISCommissionerOne'),'EISCommissionerOne','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Commissioner')),null, (select id from eg_department where lower(name) = lower('ADMINISTRATION')),(select id from eg_position where name='EIS_ADM_Commissioner'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='EISCommissionerOne'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Commissioner')) and position = (select id from eg_position where name='EIS_ADM_Commissioner') and employee = (select id from egeis_employee where code='EISCommissionerOne'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='EISCommissionerOne'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='EISCommissionerOne') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));


--PTIS Module Users

--Create REV JA

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Junior Assistant')),(select id from eg_department where lower(name)=lower('REVENUE')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'RV_JA_01',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('REVENUE')) and designation = (select id from eg_designation where lower(name) = lower('Junior Assistant'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'RV_JA_01');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'PTJAONE', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'PTJuniorAssistantOne',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='PTJAONE'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='PTJAONE') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='PTJAONE'), id from eg_role where name = 'Collection Operator' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='PTJAONE') and roleid=(select id from eg_role where name = 'Collection Operator'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='PTJAONE'), id from eg_role where name = 'Property Verifier' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='PTJAONE') and roleid=(select id from eg_role where name = 'Property Verifier'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='PTJAONE'),'PTJAONE','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Junior Assistant')),null, (select id from eg_department where lower(name) = lower('REVENUE')),(select id from eg_position where name='RV_JA_01'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='PTJAONE'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('REVENUE')) and designation = (select id from eg_designation where lower(name) = lower('Junior Assistant')) and position = (select id from eg_position where name='RV_JA_01') and employee = (select id from egeis_employee where code='PTJAONE'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='PTJAONE'),(select id from eg_boundary_type where name = 'Ward' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0, (select id from eg_boundary where name = 'Election Ward No. 1') where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='PTJAONE') and boundarytype = (select id from eg_boundary_type where name = 'Ward' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));

--Create REV BC

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Bill Collector')),(select id from eg_department where lower(name)=lower('REVENUE')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'RV_BC_01',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('REVENUE')) and designation = (select id from eg_designation where lower(name) = lower('Bill Collector'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'RV_BC_01');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'PTBCONE', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'PTBillCollectorOne',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='PTBCONE'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='PTBCONE') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='PTBCONE'), id from eg_role where name = 'Property Verifier' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='PTBCONE') and roleid=(select id from eg_role where name = 'Property Verifier'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='PTBCONE'),'PTBCONE','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Bill Collector')),null, (select id from eg_department where lower(name) = lower('REVENUE')),(select id from eg_position where name='RV_BC_01'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='PTBCONE'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('REVENUE')) and designation = (select id from eg_designation where lower(name) = lower('Bill Collector')) and position = (select id from eg_position where name='RV_BC_01') and employee = (select id from egeis_employee where code='PTBCONE'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='PTBCONE'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='PTBCONE') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));

--Create REV UD RI

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('UD Revenue Inspector')),(select id from eg_department where lower(name)=lower('REVENUE')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'RV_RI_01',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('REVENUE')) and designation = (select id from eg_designation where lower(name) = lower('UD Revenue Inspector'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'RV_RI_01');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'PTRIONE', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'PTRevenueInspectorOne',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='PTRIONE'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='PTRIONE') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='PTRIONE'), id from eg_role where name = 'Property Verifier' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='PTRIONE') and roleid=(select id from eg_role where name = 'Property Verifier'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='PTRIONE'),'PTRIONE','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('UD Revenue Inspector')),null, (select id from eg_department where lower(name) = lower('REVENUE')),(select id from eg_position where name='RV_RI_01'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='PTRIONE'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('REVENUE')) and designation = (select id from eg_designation where lower(name) = lower('UD Revenue Inspector')) and position = (select id from eg_position where name='RV_RI_01') and employee = (select id from egeis_employee where code='PTRIONE'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='PTRIONE'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='PTRIONE') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));

--Create REV RO

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Revenue Officer')),(select id from eg_department where lower(name)=lower('REVENUE')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'RV_RO_01',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('REVENUE')) and designation = (select id from eg_designation where lower(name) = lower('Revenue Officer'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'RV_RO_01');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'PTROONE', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'PTRevenueOfficerOne',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='PTROONE'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='PTROONE') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='PTROONE'), id from eg_role where name = 'Property Verifier' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='PTROONE') and roleid=(select id from eg_role where name = 'Property Verifier'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='PTROONE'),'PTROONE','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Revenue Officer')),null, (select id from eg_department where lower(name) = lower('REVENUE')),(select id from eg_position where name='RV_RO_01'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='PTROONE'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('REVENUE')) and designation = (select id from eg_designation where lower(name) = lower('Revenue Officer')) and position = (select id from eg_position where name='RV_RO_01') and employee = (select id from egeis_employee where code='PTROONE'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='PTROONE'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='PTROONE') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));

--Create ADM Commissioner

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Commissioner')),(select id from eg_department where lower(name)=lower('ADMINISTRATION')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'AD_COMM_01',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Commissioner'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'AD_COMM_01');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'PTCOMMONE', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'PTCommissionerOne',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='PTCOMMONE'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='PTCOMMONE') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='PTCOMMONE'), id from eg_role where name = 'Property Approver' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='PTCOMMONE') and roleid=(select id from eg_role where name = 'Property Approver'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='PTCOMMONE'), id from eg_role where name = 'Property Administrator' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='PTCOMMONE') and roleid=(select id from eg_role where name = 'Property Administrator'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='PTCOMMONE'), id from eg_role where name = 'Data Entry Operator' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='PTCOMMONE') and roleid=(select id from eg_role where name = 'Data Entry Operator'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='PTCOMMONE'),'PTCOMMONE','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Commissioner')),null, (select id from eg_department where lower(name) = lower('ADMINISTRATION')),(select id from eg_position where name='AD_COMM_01'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='PTCOMMONE'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Commissioner')) and position = (select id from eg_position where name='AD_COMM_01') and employee = (select id from egeis_employee where code='PTCOMMONE'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='PTCOMMONE'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='PTCOMMONE') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));

--Marriage Registration Users 

--Create ADM Commissioner

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Commissioner')),(select id from eg_department where lower(name)=lower('ADMINISTRATION')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'MR_ADM_COM_01',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Commissioner'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'MR_ADM_COM_01');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'MRCOMMONE', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'MRCOMMISSIONERONE',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='MRCOMMONE'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='MRCOMMONE') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='MRCOMMONE'), id from eg_role where name = 'Marriage Registration Approver' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='MRCOMMONE') and roleid=(select id from eg_role where name = 'Marriage Registration Approver'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='MRCOMMONE'), id from eg_role where name = 'Marriage Registration Admin' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='MRCOMMONE') and roleid=(select id from eg_role where name = 'Marriage Registration Admin'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='MRCOMMONE'), id from eg_role where name = 'Collection Operator' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='MRCOMMONE') and roleid=(select id from eg_role where name = 'Collection Operator'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='MRCOMMONE'), id from eg_role where name = 'Marriage Registration RprtViwer' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='MRCOMMONE') and roleid=(select id from eg_role where name = 'Marriage Registration RprtViwer'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='MRCOMMONE'),'MRCOMMONE','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Commissioner')),null, (select id from eg_department where lower(name) = lower('ADMINISTRATION')),(select id from eg_position where name='MR_ADM_COM_01'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='MRCOMMONE'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Commissioner')) and position = (select id from eg_position where name='MR_ADM_COM_01') and employee = (select id from egeis_employee where code='MRCOMMONE'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='MRCOMMONE'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='MRCOMMONE') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));

--Create PHS BDR

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Health Assistant/Birth and Death Registrar')),(select id from eg_department where lower(name)=lower('PUBLIC HEALTH AND SANITATION')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'PHAS_HSBDR_01',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('PUBLIC HEALTH AND SANITATION')) and designation = (select id from eg_designation where lower(name) = lower('Health Assistant/Birth and Death Registrar'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'PHAS_HSBDR_01');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'MRHABDRONE', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'MRHealthassistantOne',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='MRHABDRONE'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='MRHABDRONE') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='MRHABDRONE'), id from eg_role where name = 'Marriage Registration Creater' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='MRHABDRONE') and roleid=(select id from eg_role where name = 'Marriage Registration Creater'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='MRHABDRONE'), id from eg_role where name = 'ULB Operator' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='MRHABDRONE') and roleid=(select id from eg_role where name = 'ULB Operator'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='MRHABDRONE'), id from eg_role where name = 'Collection Operator' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='MRHABDRONE') and roleid=(select id from eg_role where name = 'Collection Operator'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='MRHABDRONE'), id from eg_role where name = 'Marriage Registration RprtViwer' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='MRHABDRONE') and roleid=(select id from eg_role where name = 'Marriage Registration RprtViwer'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='MRHABDRONE'),'MRHABDRONE','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Health Assistant/Birth and Death Registrar')),null, (select id from eg_department where lower(name) = lower('PUBLIC HEALTH AND SANITATION')),(select id from eg_position where name='PHAS_HSBDR_01'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='MRHABDRONE'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('PUBLIC HEALTH AND SANITATION')) and designation = (select id from eg_designation where lower(name) = lower('Health Assistant/Birth and Death Registrar')) and position = (select id from eg_position where name='PHAS_HSBDR_01') and employee = (select id from egeis_employee where code='MRHABDRONE'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='MRHABDRONE'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='MRHABDRONE') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));

--Council Management Module Users ***/

--Create ADM Council Clerk

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Clerk-cum-Typist')),(select id from eg_department where lower(name)=lower('ADMINISTRATION')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'ADM_JA_01',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Clerk-cum-Typist'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'ADM_JA_01');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'CMCCONE', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'CMJuniorAssistantOne',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='CMCCONE'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='CMCCONE') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='CMCCONE'), id from eg_role where name = 'Council Management Creator' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='CMCCONE') and roleid=(select id from eg_role where name = 'Council Management Creator'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='CMCCONE'), id from eg_role where name = 'Council Management Report viewer' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='CMCCONE') and roleid=(select id from eg_role where name = 'Council Management Report viewer'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='CMCCONE'),'CMCCONE','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Clerk-cum-Typist')),null, (select id from eg_department where lower(name) = lower('ADMINISTRATION')),(select id from eg_position where name='ADM_JA_01'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='CMCCONE'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Clerk-cum-Typist')) and position = (select id from eg_position where name='ADM_JA_01') and employee = (select id from egeis_employee where code='CMCCONE'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='CMCCONE'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='CMCCONE') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));

--Create ADM Commissioner

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Commissioner')),(select id from eg_department where lower(name)=lower('ADMINISTRATION')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'CM_ADM_COM_01',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Commissioner'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'CM_ADM_COM_01');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'CMCOMMONE', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'CMCommissionerOne',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='CMCOMMONE'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='CMCOMMONE') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='CMCOMMONE'), id from eg_role where name = 'Council Management Admin' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='CMCOMMONE') and roleid=(select id from eg_role where name = 'Council Management Admin'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='CMCOMMONE'), id from eg_role where name = 'Council Management Approver' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='CMCOMMONE') and roleid=(select id from eg_role where name = 'Council Management Approver'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='CMCOMMONE'), id from eg_role where name = 'Council Management Report viewer' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='CMCOMMONE') and roleid=(select id from eg_role where name = 'Council Management Report viewer'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='CMCOMMONE'),'CMCOMMONE','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Commissioner')),null, (select id from eg_department where lower(name) = lower('ADMINISTRATION')),(select id from eg_position where name='CM_ADM_COM_01'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='CMCOMMONE'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Commissioner')) and position = (select id from eg_position where name='CM_ADM_COM_01') and employee = (select id from egeis_employee where code='CMCOMMONE'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='CMCOMMONE'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='CMCOMMONE') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));

--Finance Module Users

--Create Accounts Superintendent

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Superintendent')),(select id from eg_department where lower(name)=lower('ACCOUNTS')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'ACC_AO_01',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('ACCOUNTS')) and designation = (select id from eg_designation where lower(name) = lower('Superintendent'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'ACC_AO_01');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'FNAOONE', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'ACCAccountOfficerOne',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='FNAOONE'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='FNAOONE') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='FNAOONE'), id from eg_role where name = 'Bill Creator' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='FNAOONE') and roleid=(select id from eg_role where name = 'Bill Creator'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='FNAOONE'), id from eg_role where name = 'Bill Approver' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='FNAOONE') and roleid=(select id from eg_role where name = 'Bill Approver'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='FNAOONE'), id from eg_role where name = 'Payment Creator' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='FNAOONE') and roleid=(select id from eg_role where name = 'Payment Creator'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='FNAOONE'), id from eg_role where name = 'Payment Approver' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='FNAOONE') and roleid=(select id from eg_role where name = 'Payment Approver'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='FNAOONE'), id from eg_role where name = 'Voucher Creator' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='FNAOONE') and roleid=(select id from eg_role where name = 'Voucher Creator'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='FNAOONE'), id from eg_role where name = 'Voucher Approver' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='FNAOONE') and roleid=(select id from eg_role where name = 'Voucher Approver'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='FNAOONE'), id from eg_role where name = 'Budget Creator' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='FNAOONE') and roleid=(select id from eg_role where name = 'Budget Creator'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='FNAOONE'), id from eg_role where name = 'Budget Approver' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='FNAOONE') and roleid=(select id from eg_role where name = 'Budget Approver'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='FNAOONE'), id from eg_role where name = 'Collection Operator' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='FNAOONE') and roleid=(select id from eg_role where name = 'Collection Operator'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='FNAOONE'), id from eg_role where name = 'Financial Report Viewer' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='FNAOONE') and roleid=(select id from eg_role where name = 'Financial Report Viewer'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='FNAOONE'), id from eg_role where name = 'Financial Administrator' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='FNAOONE') and roleid=(select id from eg_role where name = 'Financial Administrator'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='FNAOONE'), id from eg_role where name = 'FINANCIALS VIEW ACCESS' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='FNAOONE') and roleid=(select id from eg_role where name = 'FINANCIALS VIEW ACCESS'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='FNAOONE'),'FNAOONE','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Superintendent')),null, (select id from eg_department where lower(name) = lower('ACCOUNTS')),(select id from eg_position where name='ACC_AO_01'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='FNAOONE'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('ACCOUNTS')) and designation = (select id from eg_designation where lower(name) = lower('Superintendent')) and position = (select id from eg_position where name='ACC_AO_01') and employee = (select id from egeis_employee where code='FNAOONE'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='FNAOONE'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='FNAOONE') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));

--Create Accounts AEA 

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Assistant Examiner of Accounts')),(select id from eg_department where lower(name)=lower('ACCOUNTS')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'ACC_AEOA_01',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('ACCOUNTS')) and designation = (select id from eg_designation where lower(name) = lower('Assistant Examiner of Accounts'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'ACC_AEOA_01');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'FNAEOAONE', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'ACCAssistantExaminerofAccountsOne',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='FNAEOAONE'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='FNAEOAONE') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='FNAEOAONE'), id from eg_role where name = 'Bill Approver' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='FNAEOAONE') and roleid=(select id from eg_role where name = 'Bill Approver'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='FNAEOAONE'), id from eg_role where name = 'Payment Approver' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='FNAEOAONE') and roleid=(select id from eg_role where name = 'Payment Approver'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='FNAEOAONE'), id from eg_role where name = 'Voucher Approver' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='FNAEOAONE') and roleid=(select id from eg_role where name = 'Voucher Approver'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='FNAEOAONE'), id from eg_role where name = 'Financial Report Viewer' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='FNAEOAONE') and roleid=(select id from eg_role where name = 'Financial Report Viewer'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='FNAEOAONE'),'FNAEOAONE','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Assistant Examiner of Accounts')),null, (select id from eg_department where lower(name) = lower('ACCOUNTS')),(select id from eg_position where name='ACC_AEOA_01'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='FNAEOAONE'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('ACCOUNTS')) and designation = (select id from eg_designation where lower(name) = lower('Assistant Examiner of Accounts')) and position = (select id from eg_position where name='ACC_AEOA_01') and employee = (select id from egeis_employee where code='FNAEOAONE'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='FNAEOAONE'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='FNAEOAONE') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));

--Create Accounts EA

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Examiner of Accounts')),(select id from eg_department where lower(name)=lower('ACCOUNTS')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'ACC_EOA_01',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('ACCOUNTS')) and designation = (select id from eg_designation where lower(name) = lower('Examiner of Accounts'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'ACC_EOA_01');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'FNEOAONE', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'ACCExaminerofAccountsOne',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='FNEOAONE'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='FNEOAONE') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='FNEOAONE'), id from eg_role where name = 'Bill Approver' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='FNEOAONE') and roleid=(select id from eg_role where name = 'Bill Approver'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='FNEOAONE'), id from eg_role where name = 'Payment Approver' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='FNEOAONE') and roleid=(select id from eg_role where name = 'Payment Approver'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='FNEOAONE'), id from eg_role where name = 'Voucher Approver' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='FNEOAONE') and roleid=(select id from eg_role where name = 'Voucher Approver'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='FNEOAONE'), id from eg_role where name = 'Financial Report Viewer' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='FNEOAONE') and roleid=(select id from eg_role where name = 'Financial Report Viewer'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='FNEOAONE'),'FNEOAONE','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Examiner of Accounts')),null, (select id from eg_department where lower(name) = lower('ACCOUNTS')),(select id from eg_position where name='ACC_EOA_01'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='FNEOAONE'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('ACCOUNTS')) and designation = (select id from eg_designation where lower(name) = lower('Examiner of Accounts')) and position = (select id from eg_position where name='ACC_EOA_01') and employee = (select id from egeis_employee where code='FNEOAONE'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='FNEOAONE'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='FNEOAONE') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));

--Create Finance Commissioner

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Commissioner')),(select id from eg_department where lower(name)=lower('ADMINISTRATION')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'FN_ADM_COM_01',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Commissioner'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'FN_ADM_COM_01');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'FNCOMMONE', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'ADMCommissionerOne',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='FNCOMMONE'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='FNCOMMONE') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='FNCOMMONE'), id from eg_role where name = 'Bill Approver' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='FNCOMMONE') and roleid=(select id from eg_role where name = 'Bill Approver'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='FNCOMMONE'), id from eg_role where name = 'Payment Approver' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='FNCOMMONE') and roleid=(select id from eg_role where name = 'Payment Approver'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='FNCOMMONE'), id from eg_role where name = 'Voucher Approver' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='FNCOMMONE') and roleid=(select id from eg_role where name = 'Voucher Approver'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='FNCOMMONE'), id from eg_role where name = 'Financial Report Viewer' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='FNCOMMONE') and roleid=(select id from eg_role where name = 'Financial Report Viewer'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='FNCOMMONE'), id from eg_role where name = 'Financial Administrator' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='FNCOMMONE') and roleid=(select id from eg_role where name = 'Financial Administrator'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='FNCOMMONE'), id from eg_role where name = 'Budget Approver' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='FNCOMMONE') and roleid=(select id from eg_role where name = 'Budget Approver'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='FNCOMMONE'), id from eg_role where name = 'Collection Operator' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='FNCOMMONE') and roleid=(select id from eg_role where name = 'Collection Operator'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='FNCOMMONE'),'FNCOMMONE','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Commissioner')),null, (select id from eg_department where lower(name) = lower('ADMINISTRATION')),(select id from eg_position where name='FN_ADM_COM_01'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='FNCOMMONE'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Commissioner')) and position = (select id from eg_position where name='FN_ADM_COM_01') and employee = (select id from egeis_employee where code='FNCOMMONE'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='FNCOMMONE'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='FNCOMMONE') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));


--WTMS and STMS module users

--Create ENG JA

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Junior Assistant')),(select id from eg_department where lower(name)=lower('ENGINEERING')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'ENG_JA_001',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('ENGINEERING')) and designation = (select id from eg_designation where lower(name) = lower('Junior Assistant'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'ENG_JA_001');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'ENGJAONE', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'ENGJuniorAssistantOne',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ENGJAONE'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ENGJAONE') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ENGJAONE'), id from eg_role where name = 'COLLECTION REPORT VIEWER' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ENGJAONE') and roleid=(select id from eg_role where name = 'COLLECTION REPORT VIEWER'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ENGJAONE'), id from eg_role where name = 'Challan Creator' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ENGJAONE') and roleid=(select id from eg_role where name = 'Challan Creator'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ENGJAONE'), id from eg_role where name = 'Coll_Cancel Access' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ENGJAONE') and roleid=(select id from eg_role where name = 'Coll_Cancel Access'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ENGJAONE'), id from eg_role where name = 'Coll_Master Access' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ENGJAONE') and roleid=(select id from eg_role where name = 'Coll_Master Access'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ENGJAONE'), id from eg_role where name = 'Coll_View Access' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ENGJAONE') and roleid=(select id from eg_role where name = 'Coll_View Access'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ENGJAONE'), id from eg_role where name = 'Collection Operator' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ENGJAONE') and roleid=(select id from eg_role where name = 'Collection Operator'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ENGJAONE'), id from eg_role where name = 'Sewerage Tax Approver' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ENGJAONE') and roleid=(select id from eg_role where name = 'Sewerage Tax Approver'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ENGJAONE'), id from eg_role where name = 'Sewerage Tax Report Viewer' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ENGJAONE') and roleid=(select id from eg_role where name = 'Sewerage Tax Report Viewer'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ENGJAONE'), id from eg_role where name = 'Water Tax Approver' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ENGJAONE') and roleid=(select id from eg_role where name = 'Water Tax Approver'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ENGJAONE'), id from eg_role where name = 'Water Tax Report Viewer' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ENGJAONE') and roleid=(select id from eg_role where name = 'Water Tax Report Viewer'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ENGJAONE'), id from eg_role where name = 'Water Meter Reading Operator' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ENGJAONE') and roleid=(select id from eg_role where name = 'Water Meter Reading Operator'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='ENGJAONE'),'ENGJAONE','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Junior Assistant')),null, (select id from eg_department where lower(name) = lower('ENGINEERING')),(select id from eg_position where name='RV_JA_01'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='ENGJAONE'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('ENGINEERING')) and designation = (select id from eg_designation where lower(name) = lower('Junior Assistant')) and position = (select id from eg_position where name='RV_JA_01') and employee = (select id from egeis_employee where code='ENGJAONE'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='ENGJAONE'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='ENGJAONE') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));

--Create ENG AE

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Assistant Engineer')),(select id from eg_department where lower(name)=lower('ENGINEERING')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'ENG_AE_001',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('ENGINEERING')) and designation = (select id from eg_designation where lower(name) = lower('Assistant Engineer'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'ENG_AE_001');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'ENGAEONE', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'ENGAssistantEngineerOne',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ENGAEONE'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ENGAEONE') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ENGAEONE'), id from eg_role where name = 'Water Connection Executor' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ENGAEONE') and roleid=(select id from eg_role where name = 'Water Connection Executor'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ENGAEONE'), id from eg_role where name = 'Sewerage Connection Executor' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ENGAEONE') and roleid=(select id from eg_role where name = 'Sewerage Connection Executor'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ENGAEONE'), id from eg_role where name = 'Sewerage Tax Approver' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ENGAEONE') and roleid=(select id from eg_role where name = 'Sewerage Tax Approver'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ENGAEONE'), id from eg_role where name = 'Sewerage Tax Report Viewer' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ENGAEONE') and roleid=(select id from eg_role where name = 'Sewerage Tax Report Viewer'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ENGAEONE'), id from eg_role where name = 'Water Tax Approver' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ENGAEONE') and roleid=(select id from eg_role where name = 'Water Tax Approver'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ENGAEONE'), id from eg_role where name = 'Water Tax Report Viewer' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ENGAEONE') and roleid=(select id from eg_role where name = 'Water Tax Report Viewer'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='ENGAEONE'),'ENGAEONE','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Assistant Engineer')),null, (select id from eg_department where lower(name) = lower('ENGINEERING')),(select id from eg_position where name='ENG_AE_001'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='ENGAEONE'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('ENGINEERING')) and designation = (select id from eg_designation where lower(name) = lower('Assistant Engineer')) and position = (select id from eg_position where name='ENG_AE_001') and employee = (select id from egeis_employee where code='ENGAEONE'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='ENGAEONE'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='ENGAEONE') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));

--Create ENG DEE

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Deputy Executive Engineer')),(select id from eg_department where lower(name)=lower('ENGINEERING')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'ENG_DEE_001',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('ENGINEERING')) and designation = (select id from eg_designation where lower(name) = lower('Deputy Executive Engineer'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'ENG_DEE_001');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'ENGDEEONE', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'ENGDeputyExecutiveEngineerOne',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ENGDEEONE'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ENGDEEONE') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ENGDEEONE'), id from eg_role where name = 'Sewerage Tax Approver' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ENGDEEONE') and roleid=(select id from eg_role where name = 'Sewerage Tax Approver'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ENGDEEONE'), id from eg_role where name = 'Sewerage Tax Report Viewer' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ENGDEEONE') and roleid=(select id from eg_role where name = 'Sewerage Tax Report Viewer'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ENGDEEONE'), id from eg_role where name = 'Water Tax Approver' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ENGDEEONE') and roleid=(select id from eg_role where name = 'Water Tax Approver'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ENGDEEONE'), id from eg_role where name = 'Water Tax Report Viewer' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ENGDEEONE') and roleid=(select id from eg_role where name = 'Water Tax Report Viewer'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='ENGDEEONE'),'ENGDEEONE','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Deputy Executive Engineer')),null, (select id from eg_department where lower(name) = lower('ENGINEERING')),(select id from eg_position where name='ENG_DEE_001'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='ENGDEEONE'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('ENGINEERING')) and designation = (select id from eg_designation where lower(name) = lower('Deputy Executive Engineer')) and position = (select id from eg_position where name='ENG_DEE_001') and employee = (select id from egeis_employee where code='ENGDEEONE'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='ENGDEEONE'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='ENGDEEONE') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));

--Create ENG EE

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Executive Engineer')),(select id from eg_department where lower(name)=lower('ENGINEERING')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'ENG_EE_001',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('ENGINEERING')) and designation = (select id from eg_designation where lower(name) = lower('Executive Engineer'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'ENG_EE_001');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'ENGEEONE', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'ENGExecutiveEngineerOne',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ENGEEONE'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ENGEEONE') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ENGEEONE'), id from eg_role where name = 'Sewerage Tax Approver' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ENGEEONE') and roleid=(select id from eg_role where name = 'Sewerage Tax Approver'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ENGEEONE'), id from eg_role where name = 'Sewerage Tax Report Viewer' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ENGEEONE') and roleid=(select id from eg_role where name = 'Sewerage Tax Report Viewer'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ENGEEONE'), id from eg_role where name = 'Water Tax Approver' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ENGEEONE') and roleid=(select id from eg_role where name = 'Water Tax Approver'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ENGEEONE'), id from eg_role where name = 'Water Tax Report Viewer' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ENGEEONE') and roleid=(select id from eg_role where name = 'Water Tax Report Viewer'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='ENGEEONE'),'ENGEEONE','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Executive Engineer')),null, (select id from eg_department where lower(name) = lower('ENGINEERING')),(select id from eg_position where name='ENG_EE_001'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='ENGEEONE'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('ENGINEERING')) and designation = (select id from eg_designation where lower(name) = lower('Executive Engineer')) and position = (select id from eg_position where name='ENG_EE_001') and employee = (select id from egeis_employee where code='ENGEEONE'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='ENGEEONE'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='ENGEEONE') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));

--Create ADM CoMM

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Commissioner')),(select id from eg_department where lower(name)=lower('ADMINISTRATION')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'WS_ADM_COM_001',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Commissioner'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'WS_ADM_COM_001');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'ADMCOMONE', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'ADMCommissionerOne',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADMCOMONE'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADMCOMONE') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADMCOMONE'), id from eg_role where name = 'Sewerage Tax Approver' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADMCOMONE') and roleid=(select id from eg_role where name = 'Sewerage Tax Approver'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADMCOMONE'), id from eg_role where name = 'Sewerage Tax Report Viewer' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADMCOMONE') and roleid=(select id from eg_role where name = 'Sewerage Tax Report Viewer'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADMCOMONE'), id from eg_role where name = 'Water Tax Approver' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADMCOMONE') and roleid=(select id from eg_role where name = 'Water Tax Approver'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADMCOMONE'), id from eg_role where name = 'Water Tax Report Viewer' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADMCOMONE') and roleid=(select id from eg_role where name = 'Water Tax Report Viewer'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='ADMCOMONE'),'ADMCOMONE','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Commissioner')),null, (select id from eg_department where lower(name) = lower('ADMINISTRATION')),(select id from eg_position where name='WS_ADM_COM_001'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='ADMCOMONE'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Commissioner')) and position = (select id from eg_position where name='WS_ADM_COM_001') and employee = (select id from egeis_employee where code='ADMCOMONE'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='ADMCOMONE'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='ADMCOMONE') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));

--Collections module Users
--Create ADM JA

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Junior Assistant')),(select id from eg_department where lower(name)=lower('ADMINISTRATION')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'ADM_JA_001',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Junior Assistant'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'ADM_JA_001');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'ADMJAONE', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'ADMJuniorAssistantOne',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADMJAONE'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADMJAONE') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADMJAONE'), id from eg_role where name = 'COLLECTION REPORT VIEWER' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADMJAONE') and roleid=(select id from eg_role where name = 'COLLECTION REPORT VIEWER'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADMJAONE'), id from eg_role where name = 'Challan Creator' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADMJAONE') and roleid=(select id from eg_role where name = 'Challan Creator'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADMJAONE'), id from eg_role where name = 'Challan Creator' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADMJAONE') and roleid=(select id from eg_role where name = 'ULB Operator'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADMJAONE'), id from eg_role where name = 'Coll_Cancel Access' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADMJAONE') and roleid=(select id from eg_role where name = 'Coll_Cancel Access'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADMJAONE'), id from eg_role where name = 'Coll_Master Access' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADMJAONE') and roleid=(select id from eg_role where name = 'Coll_Master Access'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADMJAONE'), id from eg_role where name = 'Coll_View Access' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADMJAONE') and roleid=(select id from eg_role where name = 'Coll_View Access'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADMJAONE'), id from eg_role where name = 'Collection Operator' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADMJAONE') and roleid=(select id from eg_role where name = 'Collection Operator'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADMJAONE'), id from eg_role where name = 'Advertisement Tax Creator' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADMJAONE') and roleid=(select id from eg_role where name = 'Advertisement Tax Creator'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADMJAONE'), id from eg_role where name = 'Advertisement Tax Report Viewer' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADMJAONE') and roleid=(select id from eg_role where name = 'Advertisement Tax Report Viewer'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='ADMJAONE'),'ADMJAONE','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Junior Assistant')),null, (select id from eg_department where lower(name) = lower('ADMINISTRATION')),(select id from eg_position where name='ADM_JA_001'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='ADMJAONE'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Junior Assistant')) and position = (select id from eg_position where name='ADM_JA_001') and employee = (select id from egeis_employee where code='ADMJAONE'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='ADMJAONE'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='ADMJAONE') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));

--Create Manager Assignment

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Manager')),(select id from eg_department where lower(name)=lower('ADMINISTRATION')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'ADM_MNG_001',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Manager'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'ADM_MNG_001');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'ADMMNGONE', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'ADMManagerOne',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADMMNGONE'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADMMNGONE') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADMMNGONE'), id from eg_role where name = 'COLLECTION REPORT VIEWER' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADMMNGONE') and roleid=(select id from eg_role where name = 'COLLECTION REPORT VIEWER'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADMMNGONE'), id from eg_role where name = 'Challan Creator' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADMMNGONE') and roleid=(select id from eg_role where name = 'Challan Creator'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADMMNGONE'), id from eg_role where name = 'Coll_Cancel Access' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADMMNGONE') and roleid=(select id from eg_role where name = 'Coll_Cancel Access'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADMMNGONE'), id from eg_role where name = 'Coll_Master Access' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADMMNGONE') and roleid=(select id from eg_role where name = 'Coll_Master Access'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADMMNGONE'), id from eg_role where name = 'Coll_View Access' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADMMNGONE') and roleid=(select id from eg_role where name = 'Coll_View Access'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADMMNGONE'), id from eg_role where name = 'Collection Operator' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADMMNGONE') and roleid=(select id from eg_role where name = 'Collection Operator'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADMMNGONE'), id from eg_role where name = 'Remitter' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADMMNGONE') and roleid=(select id from eg_role where name = 'Remitter'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='ADMMNGONE'),'ADMMNGONE','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Manager')),null, (select id from eg_department where lower(name) = lower('ADMINISTRATION')),(select id from eg_position where name='ADM_MNG_001'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='ADMMNGONE'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Manager')) and position = (select id from eg_position where name='ADM_MNG_001') and employee = (select id from egeis_employee where code='ADMMNGONE'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='ADMMNGONE'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='ADMMNGONE') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));

--Create ADM SA 

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Senior Assistant')),(select id from eg_department where lower(name)=lower('ADMINISTRATION')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'ADM_SA_001',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Senior Assistant'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'ADM_SA_001');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'ADMSAONE', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'ADMSAOne',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADMSAONE'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADMSAONE') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADMSAONE'), id from eg_role where name = 'COLLECTION REPORT VIEWER' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADMSAONE') and roleid=(select id from eg_role where name = 'COLLECTION REPORT VIEWER'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADMSAONE'), id from eg_role where name = 'Challan Creator' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADMSAONE') and roleid=(select id from eg_role where name = 'Challan Creator'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADMSAONE'), id from eg_role where name = 'Coll_Cancel Access' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADMSAONE') and roleid=(select id from eg_role where name = 'Coll_Cancel Access'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADMSAONE'), id from eg_role where name = 'Coll_Master Access' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADMSAONE') and roleid=(select id from eg_role where name = 'Coll_Master Access'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADMSAONE'), id from eg_role where name = 'Coll_View Access' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADMSAONE') and roleid=(select id from eg_role where name = 'Coll_View Access'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='ADMSAONE'),'ADMSAONE','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Senior Assistant')),null, (select id from eg_department where lower(name) = lower('ADMINISTRATION')),(select id from eg_position where name='ADM_SA_001'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='ADMSAONE'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Senior Assistant')) and position = (select id from eg_position where name='ADM_SA_001') and employee = (select id from egeis_employee where code='ADMSAONE'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='ADMSAONE'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='ADMSAONE') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));



--Adtax Users

--ADM JA created as Collection Operator

insert into egeis_deptdesig(id, designation, department, outsourcedposts, sanctionedposts, version, createddate, lastmodifieddate, createdby, lastmodifiedby) values(nextval('seq_egeis_deptdesig'),(select id from eg_designation where lower(name)=lower('Commissioner')),(select id from eg_department where lower(name)=lower('ADMINISTRATION')),0,0,0,now(),now(),1,1) on conflict do nothing;

insert into eg_position (id,name,deptdesig,createddate,lastmodifieddate,createdby,lastmodifiedby,ispostoutsourced,version) select nextval('seq_eg_position'),'AD_ADM_COM_01',(select id from egeis_deptdesig where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Commissioner'))), now(),now(),1,1,false,0 where not exists (select id from eg_position where name = 'AD_ADM_COM_01');

insert into eg_user (id, title, salutation, dob,locale, username, password, pwdexpirydate, mobilenumber, altcontactnumber, emailid, createddate, lastmodifieddate, createdby, lastmodifiedby, active, name, gender, pan, aadhaarnumber, type, version, guardian, guardianrelation, signature, accountlocked) 
values (nextval('seq_eg_user'),null,null,null,null,'ADTCOMMONE', '$2a$10$IEkvxGAxbQsdNULjNe.yZOFP/yhcf/WkT32sJwKw7PDZyGimfgzk6',current_date+1000,'0123456789','0123456789',null, now(), now(),1,1,true,'ADTCommissionerOne',1,null,null,'EMPLOYEE',0,null,null,null,false) on conflict do nothing;

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADTCOMMONE'), id from eg_role where name = 'EMPLOYEE' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADTCOMMONE') and roleid=(select id from eg_role where name = 'EMPLOYEE'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADTCOMMONE'), id from eg_role where name = 'Advertisement Tax Report Viewer' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADTCOMMONE') and roleid=(select id from eg_role where name = 'Advertisement Tax Report Viewer'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADTCOMMONE'), id from eg_role where name = 'Advertisement Tax Approver' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADTCOMMONE') and roleid=(select id from eg_role where name = 'Advertisement Tax Approver'));

insert into eg_userrole (userid,roleid) select (select id from eg_user where username='ADTCOMMONE'), id from eg_role where name = 'Advertisement Tax Admin' and not exists (select userid from eg_userrole where userid = (select id from eg_user where username='ADTCOMMONE') and roleid=(select id from eg_role where name = 'Advertisement Tax Admin'));

insert into egeis_employee (id, code, dateofappointment, dateofretirement, employeestatus, employeetype, version) values ((select id from eg_user where username='ADTCOMMONE'),'ADTCOMMONE','1993-05-31',null,'EMPLOYED',1,0) on conflict do nothing;

insert into egeis_assignment (id, fund, function, designation, functionary, department, position, grade, lastmodifiedby, lastmodifieddate, createddate, createdby, fromdate, todate, version, employee, isprimary) select nextval('seq_egeis_assignment'),null,null,(select id from eg_designation where lower(name) = lower('Commissioner')),null, (select id from eg_department where lower(name) = lower('ADMINISTRATION')),(select id from eg_position where name='AD_ADM_COM_01'),null,1,now(),now(),1,'2015-01-01','2020-01-01',0,(select id from egeis_employee where code='ADTCOMMONE'),true where not exists (select id from egeis_assignment where department = (select id from eg_department where lower(name) = lower('ADMINISTRATION')) and designation = (select id from eg_designation where lower(name) = lower('Commissioner')) and position = (select id from eg_position where name='AD_ADM_COM_01') and employee = (select id from egeis_employee where code='ADTCOMMONE'));

insert into egeis_jurisdiction(id, employee, boundarytype, createddate, lastmodifieddate, createdby, lastmodifiedby, version, boundary) select nextval('seq_egeis_jurisdiction'),(select id from egeis_employee where code='ADTCOMMONE'),(select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')),now(),now(),1,1,0,2 where not exists (select id from egeis_jurisdiction where employee = (select id from egeis_employee where code='ADTCOMMONE') and boundarytype = (select id from eg_boundary_type where name = 'City' and hierarchytype = (select id from eg_hierarchy_type where name='ADMINISTRATION')));


--To enable Sewerage Tax Module

update eg_module set enabled = 't' where displayname = 'Sewerage Tax Management';

--To enable Advertisement Tax module

update eg_module set enabled = 't' where displayname = 'Advertisement Tax';

-- Citizen userrole mapping
insert into eg_userrole values((select id from eg_role where name = 'CITIZEN'),(select id from eg_user where username = '9999999999'));

-- To enable Data entry screen for PTIS

update eg_action set enabled = 't' where displayname = 'Data Entry Screen' and contextroot = 'ptis';

--To disble digital signature for TL module

update egtl_configuration set value ='false' where key='INCLUDE_DIGITAL_SIGN_WORKFLOW';

