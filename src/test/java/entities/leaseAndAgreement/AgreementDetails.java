package entities.leaseAndAgreement;

public class AgreementDetails {

    private String tenderNumber;

    private String tenderDate;

    private String natureOfAllotment;

    private String councilNumber;

    private String councilDate;

    private String landRent;

    private String paymentCycle;

    private String bankGuaranteeAmount;

    private String bankGuaranteeDate;

    private String solvencyCertificateNumber;

    private String solvencyCertificateDate;

    private String securityDeposit;

    private String securityDepositDate;

    private String commencementDate;

    private String rentIncrementMethod;

    private String timePeriod;

    public String getTenderNumber() {
        return tenderNumber;
    }

    public void setTenderNumber(String tenderNumber) {
        this.tenderNumber = tenderNumber;
    }

    public String getTenderDate() {
        return tenderDate;
    }

    public void setTenderDate(String tenderDate) {
        this.tenderDate = tenderDate;
    }

    public String getNatureOfAllotment() {
        return natureOfAllotment;
    }

    public void setNatureOfAllotment(String natureOfAllotment) {
        this.natureOfAllotment = natureOfAllotment;
    }

    public String getCouncilNumber() {
        return councilNumber;
    }

    public void setCouncilNumber(String councilNumber) {
        this.councilNumber = councilNumber;
    }

    public String getCouncilDate() {
        return councilDate;
    }

    public void setCouncilDate(String councilDate) {
        this.councilDate = councilDate;
    }

    public String getLandRent() {
        return landRent;
    }

    public void setLandRent(String landRent) {
        this.landRent = landRent;
    }

    public String getPaymentCycle() {
        return paymentCycle;
    }

    public void setPaymentCycle(String paymentCycle) {
        this.paymentCycle = paymentCycle;
    }

    public String getBankGuaranteeAmount() {
        return bankGuaranteeAmount;
    }

    public void setBankGuaranteeAmount(String bankGuaranteeAmount) {
        this.bankGuaranteeAmount = bankGuaranteeAmount;
    }

    public String getBankGuaranteeDate() {
        return bankGuaranteeDate;
    }

    public void setBankGuaranteeDate(String bankGuaranteeDate) {
        this.bankGuaranteeDate = bankGuaranteeDate;
    }

    public String getSolvencyCertificateNumber() {
        return solvencyCertificateNumber;
    }

    public void setSolvencyCertificateNumber(String solvencyCertificateNumber) {
        this.solvencyCertificateNumber = solvencyCertificateNumber;
    }

    public String getSolvencyCertificateDate() {
        return solvencyCertificateDate;
    }

    public void setSolvencyCertificateDate(String solvencyCertificateDate) {
        this.solvencyCertificateDate = solvencyCertificateDate;
    }

    public String getSecurityDeposit() {
        return securityDeposit;
    }

    public void setSecurityDeposit(String securityDeposit) {
        this.securityDeposit = securityDeposit;
    }

    public String getSecurityDepositDate() {
        return securityDepositDate;
    }

    public void setSecurityDepositDate(String securityDepositDate) {
        this.securityDepositDate = securityDepositDate;
    }

    public String getCommencementDate() {
        return commencementDate;
    }

    public void setCommencementDate(String commencementDate) {
        this.commencementDate = commencementDate;
    }

    public String getRentIncrementMethod() {
        return rentIncrementMethod;
    }

    public void setRentIncrementMethod(String rentIncrementMethod) {
        this.rentIncrementMethod = rentIncrementMethod;
    }

    public String getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(String timePeriod) {
        this.timePeriod = timePeriod;
    }
}
