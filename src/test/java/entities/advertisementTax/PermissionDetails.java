package entities.advertisementTax;

public class PermissionDetails {

    private String adParticular;
    private String owner;
    private String advertisementDuration;

    public String getAdParticular() {
        return adParticular;
    }

    public void setAdParticular(String adParticular) {
        this.adParticular = adParticular;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getAdvertisementDuration() {
        return advertisementDuration;
    }

    public void setAdvertisementDuration(String advertisementDuration) {
        this.advertisementDuration = advertisementDuration;
    }
}
