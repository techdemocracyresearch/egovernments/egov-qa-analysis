package entities.marriageRegistration;

public class MarriageRegistrationInformation {

    private String RegistrationUnit;
    private String Street;
    private String Locality;
    private String City;
    private String DateOfMarriage;
    private String VenueOfMarriage;
    private String PlaceOfMarriage;
    private String FullName;
    private String FathersMothersName;
    private String Religion;
    private String StatusAtTheTimeMarriage;
    private String ResidenceAddress;
    private String OfficeAddress;
    private String PhoneNo;
    private String Occupation;
    private String EducationQualification;
    private String Nationality;

    //        private String WifeFullName;
//        private String WifeReligion;
    private String WifeStatusAtTheTimeMarriage;
    private String WifeResidenceAddress;
    private String WifeOfficeAddress;
    private String WifePhoneNo;
    private String WifeOccupation;
    private String WifeEducationQualification;
    private String WifeNationality;

//    public String getWifeFullName() {
//        return WifeFullName;
//    }
//
//    public void setWifeFullName(String wifeFullName) {
//        WifeFullName = wifeFullName;
//    }
//
//    public String getWifeReligion() {
//        return WifeReligion;
//    }
//
//    public void setWifeReligion(String wifeReligion) {
//        WifeReligion = wifeReligion;
//    }

    public String getWifeStatusAtTheTimeMarriage() {
        return WifeStatusAtTheTimeMarriage;
    }

    public void setWifeStatusAtTheTimeMarriage(String wifeStatusAtTheTimeMarriage) {
        WifeStatusAtTheTimeMarriage = wifeStatusAtTheTimeMarriage;
    }

    public String getWifeResidenceAddress() {
        return WifeResidenceAddress;
    }

    public void setWifeResidenceAddress(String wifeResidenceAddress) {
        WifeResidenceAddress = wifeResidenceAddress;
    }

    public String getWifeOfficeAddress() {
        return WifeOfficeAddress;
    }

    public void setWifeOfficeAddress(String wifeOfficeAddress) {
        WifeOfficeAddress = wifeOfficeAddress;
    }

    public String getWifePhoneNo() {
        return WifePhoneNo;
    }

    public void setWifePhoneNo(String wifePhoneNo) {
        WifePhoneNo = wifePhoneNo;
    }

    public String getWifeOccupation() {
        return WifeOccupation;
    }

    public void setWifeOccupation(String wifeOccupation) {
        WifeOccupation = wifeOccupation;
    }

    public String getWifeEducationQualification() {
        return WifeEducationQualification;
    }

    public void setWifeEducationQualification(String wifeEducationQualification) {
        WifeEducationQualification = wifeEducationQualification;
    }

    public String getWifeNationality() {
        return WifeNationality;
    }

    public void setWifeNationality(String wifeNationality) {
        WifeNationality = wifeNationality;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getFathersMothersName() {
        return FathersMothersName;
    }

    public void setFathersMothersName(String fathersMothersName) {
        FathersMothersName = fathersMothersName;
    }

    public String getReligion() {
        return Religion;
    }

    public void setReligion(String religion) {
        Religion = religion;
    }

    public String getStatusAtTheTimeMarriage() {
        return StatusAtTheTimeMarriage;
    }

    public void setStatusAtTheTimeMarriage(String statusAtTheTimeMarriage) {
        StatusAtTheTimeMarriage = statusAtTheTimeMarriage;
    }

    public String getResidenceAddress() {
        return ResidenceAddress;
    }

    public void setResidenceAddress(String residenceAddress) {
        ResidenceAddress = residenceAddress;
    }

    public String getOfficeAddress() {
        return OfficeAddress;
    }

    public void setOfficeAddress(String officeAddress) {
        OfficeAddress = officeAddress;
    }

    public String getPhoneNo() {
        return PhoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        PhoneNo = phoneNo;
    }

    public String getOccupation() {
        return Occupation;
    }

    public void setOccupation(String occupation) {
        Occupation = occupation;
    }

    public String getEducationQualification() {
        return EducationQualification;
    }

    public void setEducationQualification(String educationQualification) {
        EducationQualification = educationQualification;
    }

    public String getNationality() {
        return Nationality;
    }

    public void setNationality(String nationality) {
        Nationality = nationality;
    }

    public String getRegistrationUnit() {
        return RegistrationUnit;
    }

    public void setRegistrationUnit(String registrationUnit) {
        RegistrationUnit = registrationUnit;
    }

    public String getStreet() {
        return Street;
    }

    public void setStreet(String street) {
        Street = street;
    }

    public String getLocality() {
        return Locality;
    }

    public void setLocality(String locality) {
        Locality = locality;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getDateOfMarriage() {
        return DateOfMarriage;
    }

    public void setDateOfMarriage(String dateOfMarriage) {
        DateOfMarriage = dateOfMarriage;
    }

    public String getVenueOfMarriage() {
        return VenueOfMarriage;
    }

    public void setVenueOfMarriage(String venueOfMarriage) {
        VenueOfMarriage = venueOfMarriage;
    }

    public String getPlaceOfMarriage() {
        return PlaceOfMarriage;
    }

    public void setPlaceOfMarriage(String placeOfMarriage) {
        PlaceOfMarriage = placeOfMarriage;
    }
}
