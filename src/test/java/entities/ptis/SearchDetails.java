package entities.ptis;

public class SearchDetails {

    private String searchValue1;
    private String searchValue2;
    private String searchValue3;
    private String searchValue4;

    public String getSearchValue3() {
        return searchValue3;
    }

    public void setSearchValue3(String searchValue3) {
        this.searchValue3 = searchValue3;
    }

    public String getSearchValue4() {
        return searchValue4;
    }

    public void setSearchValue4(String searchValue4) {
        this.searchValue4 = searchValue4;
    }

    public String getSearchValue1() {
        return searchValue1;
    }

    public void setSearchValue1(String searchValue1) {
        this.searchValue1 = searchValue1;
    }

    public String getSearchValue2() {
        return searchValue2;
    }

    public void setSearchValue2(String searchValue2) {
        this.searchValue2 = searchValue2;
    }
}
