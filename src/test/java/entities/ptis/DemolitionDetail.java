package entities.ptis;

/**
 * Created by bimal on 3/3/17.
 */
public class DemolitionDetail {
    private String ReasonForDemolition;
    private String SurveyNumber;
    private String PattaNumber;
    private String MarketValue;
    private String CapitalValue;
    private String North;
    private String East;
    private String West;
    private String South;

    public String getReasonForDemolition() {
        return ReasonForDemolition;
    }

    public void setReasonForDemolition(String reasonForDemolition) {
        ReasonForDemolition = reasonForDemolition;
    }

    public String getSurveyNumber() {
        return SurveyNumber;
    }

    public void setSurveyNumber(String surveyNumber) {
        SurveyNumber = surveyNumber;
    }

    public String getPattaNumber() {
        return PattaNumber;
    }

    public void setPattaNumber(String pattaNumber) {
        PattaNumber = pattaNumber;
    }

    public String getMarketValue() {
        return MarketValue;
    }

    public void setMarketValue(String marketValue) {
        MarketValue = marketValue;
    }

    public String getCapitalValue() {
        return CapitalValue;
    }

    public void setCapitalValue(String capitalValue) {
        CapitalValue = capitalValue;
    }

    public String getNorth() {
        return North;
    }

    public void setNorth(String north) {
        North = north;
    }

    public String getEast() {
        return East;
    }

    public void setEast(String east) {
        East = east;
    }

    public String getWest() {
        return West;
    }

    public void setWest(String west) {
        West = west;
    }

    public String getSouth() {
        return South;
    }

    public void setSouth(String south) {
        South = south;
    }
}
