package entities.ptis;

public class RegistrationDetails {

    private String sellerExecutantName;
    private String buyerClaimantName;
    private String doorNo;
    private String propertyAddress;
    private String registeredPlotArea;
    private String registeredPlinthArea;
    private String eastBoundary;
    private String westBoundary;
    private String northBoundary;
    private String southBoundary;
    private String sroName;
    private String reasonForChange;
    private String registrationDocumentNumber;
    private String registrationDocumentDate;
    private String partiesConsiderationValue;
    private String departmentGuidelinesValue;

    public String getSellerExecutantName() {
        return sellerExecutantName;
    }

    public void setSellerExecutantName(String sellerExecutantName) {
        this.sellerExecutantName = sellerExecutantName;
    }

    public String getBuyerClaimantName() {
        return buyerClaimantName;
    }

    public void setBuyerClaimantName(String buyerClaimantName) {
        this.buyerClaimantName = buyerClaimantName;
    }

    public String getDoorNo() {
        return doorNo;
    }

    public void setDoorNo(String doorNo) {
        this.doorNo = doorNo;
    }

    public String getPropertyAddress() {
        return propertyAddress;
    }

    public void setPropertyAddress(String propertyAddress) {
        this.propertyAddress = propertyAddress;
    }

    public String getRegisteredPlotArea() {
        return registeredPlotArea;
    }

    public void setRegisteredPlotArea(String registeredPlotArea) {
        this.registeredPlotArea = registeredPlotArea;
    }

    public String getRegisteredPlinthArea() {
        return registeredPlinthArea;
    }

    public void setRegisteredPlinthArea(String registeredPlinthArea) {
        this.registeredPlinthArea = registeredPlinthArea;
    }

    public String getEastBoundary() {
        return eastBoundary;
    }

    public void setEastBoundary(String eastBoundary) {
        this.eastBoundary = eastBoundary;
    }

    public String getWestBoundary() {
        return westBoundary;
    }

    public void setWestBoundary(String westBoundary) {
        this.westBoundary = westBoundary;
    }

    public String getNorthBoundary() {
        return northBoundary;
    }

    public void setNorthBoundary(String northBoundary) {
        this.northBoundary = northBoundary;
    }

    public String getSouthBoundary() {
        return southBoundary;
    }

    public void setSouthBoundary(String southBoundary) {
        this.southBoundary = southBoundary;
    }


    public String getSroName() {
        return sroName;
    }

    public void setSroName(String sroName) {
        this.sroName = sroName;
    }

    public String getReasonForChange() {
        return reasonForChange;
    }

    public void setReasonForChange(String reasonForChange) {
        this.reasonForChange = reasonForChange;
    }

    public String getRegistrationDocumentNumber() {
        return registrationDocumentNumber;
    }

    public void setRegistrationDocumentNumber(String registrationDocumentNumber) {
        this.registrationDocumentNumber = registrationDocumentNumber;
    }

    public String getRegistrationDocumentDate() {
        return registrationDocumentDate;
    }

    public void setRegistrationDocumentDate(String registrationDocumentDate) {
        this.registrationDocumentDate = registrationDocumentDate;
    }

    public String getPartiesConsiderationValue() {
        return partiesConsiderationValue;
    }

    public void setPartiesConsiderationValue(String partiesConsiderationValue) {
        this.partiesConsiderationValue = partiesConsiderationValue;
    }

    public String getDepartmentGuidelinesValue() {
        return departmentGuidelinesValue;
    }

    public void setDepartmentGuidelinesValue(String departmentGuidelinesValue) {
        this.departmentGuidelinesValue = departmentGuidelinesValue;
    }
}
