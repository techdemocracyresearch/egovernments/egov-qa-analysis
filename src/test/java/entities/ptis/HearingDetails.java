package entities.ptis;

public class HearingDetails {
    private String hearingDate;
    private String hearingTime;
    private String venue;

    public String getHearingDate() {
        return hearingDate;
    }

    public void setHearingDate(String hearingDate) {
        this.hearingDate = hearingDate;
    }

    public String getHearingTime() {
        return hearingTime;
    }

    public void setHearingTime(String hearingTime) {
        this.hearingTime = hearingTime;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }
}
