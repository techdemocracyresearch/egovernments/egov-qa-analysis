package entities.ptis;

public class RevisionPetitionDetails {

    private String revisionPetitionDetail;

    public String getRevisionPetitionDetail() {
        return revisionPetitionDetail;
    }

    public void setRevisionPetitionDetail(String revisionPetitionDetail) {
        this.revisionPetitionDetail = revisionPetitionDetail;
    }
}
