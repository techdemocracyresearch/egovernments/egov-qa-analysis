package entities.employeeManagement.createEmployee;

public class AssignmentDetails {

    private String isPrimary;
    private String fromDate;
    private String toDate;
    private String department;
    private String designation;
    private String position;
    private String grade;
    private String function;
    private String functionary;
    private String fund;
    private boolean isHOD;
    private String govtOrderNumber;
    private String roles;

    public String getRoles() { return roles; }

    public void setRoles(String roles) { this.roles = roles; }

    public String getIsPrimary() {
        return isPrimary;
    }

    public void setIsPrimary(String isPrimary) {
        this.isPrimary = isPrimary;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getPosition() { return position; }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public String getFunctionary() {
        return functionary;
    }

    public void setFunctionary(String functionary) {
        this.functionary = functionary;
    }

    public String getFund() {
        return fund;
    }

    public void setFund(String fund) {
        this.fund = fund;
    }

    public boolean getIsHOD() {
        return isHOD;
    }

    public void setIsHOD(boolean isHOD) {
        this.isHOD = isHOD;
    }

    public String getGovtOrderNumber() {
        return govtOrderNumber;
    }

    public void setGovtOrderNumber(String govtOrderNumber) {
        this.govtOrderNumber = govtOrderNumber;
    }
}
