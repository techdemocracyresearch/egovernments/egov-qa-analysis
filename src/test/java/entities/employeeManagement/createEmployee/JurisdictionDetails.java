package entities.employeeManagement.createEmployee;

public class JurisdictionDetails {

    private String jurisdictionType;
    private String jurisdictionList;

    public String getJurisdictionType() {
        return jurisdictionType;
    }

    public void setJurisdictionType(String jurisdictionType) {
        this.jurisdictionType = jurisdictionType;
    }

    public String getJurisdictionList() {
        return jurisdictionList;
    }

    public void setJurisdictionList(String jurisdictionList) {
        this.jurisdictionList = jurisdictionList;
    }
}
