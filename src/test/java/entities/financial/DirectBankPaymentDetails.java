package entities.financial;

public class DirectBankPaymentDetails {

    private String fundId;
    private String voucherDepartment;
    private String voucherFunction;
    private String bankBranch;
    private String amount;
    private String accountNumber;
    private String accountCode1;
    private String debitAmount1;
    private String ledgerAccount1;
    private String ledgerType1;
    private String ledgerCode1;
    private String ledgerAmount1;

    public String getFundId() {
        return fundId;
    }

    public void setFundId(String fundId) {
        this.fundId = fundId;
    }

    public String getVoucherDepartment() {
        return voucherDepartment;
    }

    public void setVoucherDepartment(String voucherDepartment) {
        this.voucherDepartment = voucherDepartment;
    }

    public String getVoucherFunction() {
        return voucherFunction;
    }

    public void setVoucherFunction(String voucherFunction) {
        this.voucherFunction = voucherFunction;
    }

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountCode1() {
        return accountCode1;
    }

    public void setAccountCode1(String accountCode1) {
        this.accountCode1 = accountCode1;
    }

    public String getDebitAmount1() {
        return debitAmount1;
    }

    public void setDebitAmount1(String debitAmount1) {
        this.debitAmount1 = debitAmount1;
    }

    public String getLedgerAccount1() {
        return ledgerAccount1;
    }

    public void setLedgerAccount1(String ledgerAccount1) {
        this.ledgerAccount1 = ledgerAccount1;
    }

    public String getLedgerType1() {
        return ledgerType1;
    }

    public void setLedgerType1(String ledgerType1) {
        this.ledgerType1 = ledgerType1;
    }

    public String getLedgerCode1() {
        return ledgerCode1;
    }

    public void setLedgerCode1(String ledgerCode1) {
        this.ledgerCode1 = ledgerCode1;
    }

    public String getLedgerAmount1() {
        return ledgerAmount1;
    }

    public void setLedgerAmount1(String ledgerAmount1) {
        this.ledgerAmount1 = ledgerAmount1;
    }
}
