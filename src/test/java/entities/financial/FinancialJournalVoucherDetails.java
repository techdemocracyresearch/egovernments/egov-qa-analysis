package entities.financial;

public class FinancialJournalVoucherDetails {

    private String voucherType;
    private String fundId;
    private String accountCode1;
    private String accountCode2;
    private String accountCode3;
    private String department;
    private String function;
    private String debitAmount1;
    private String creditAmount2;
    private String creditAmount3;
    private String ledgerAmount1;
    private String ledgerAmount2;
    private String ledgerType1;
    private String ledgerType2;
    private String ledgerCode1;
    private String ledgerCode2;

    public String getDebitAmount1() {
        return debitAmount1;
    }

    public void setDebitAmount1(String debitAmount1) {
        this.debitAmount1 = debitAmount1;
    }

    public String getCreditAmount2() {
        return creditAmount2;
    }

    public void setCreditAmount2(String creditAmount2) {
        this.creditAmount2 = creditAmount2;
    }

    public String getCreditAmount3() {
        return creditAmount3;
    }

    public void setCreditAmount3(String creditAmount3) {
        this.creditAmount3 = creditAmount3;
    }

    public String getLedgerAmount1() {
        return ledgerAmount1;
    }

    public void setLedgerAmount1(String ledgerAmount1) {
        this.ledgerAmount1 = ledgerAmount1;
    }

    public String getLedgerAmount2() {
        return ledgerAmount2;
    }

    public void setLedgerAmount2(String ledgerAmount2) {
        this.ledgerAmount2 = ledgerAmount2;
    }

    public String getLedgerType1() {
        return ledgerType1;
    }

    public void setLedgerType1(String ledgerType1) {
        this.ledgerType1 = ledgerType1;
    }

    public String getLedgerType2() {
        return ledgerType2;
    }

    public void setLedgerType2(String ledgerType2) {
        this.ledgerType2 = ledgerType2;
    }

    public String getLedgerCode1() {
        return ledgerCode1;
    }

    public void setLedgerCode1(String ledgerCode1) {
        this.ledgerCode1 = ledgerCode1;
    }

    public String getLedgerCode2() {
        return ledgerCode2;
    }

    public void setLedgerCode2(String ledgerCode2) {
        this.ledgerCode2 = ledgerCode2;
    }

    public String getFundId() {
        return fundId;
    }

    public void setFundId(String fundId) {
        this.fundId = fundId;
    }

    public String getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(String voucherType) {
        this.voucherType = voucherType;
    }

    public String getAccountCode1() {
        return accountCode1;
    }

    public void setAccountCode1(String accountCode1) {
        this.accountCode1 = accountCode1;
    }

    public String getAccountCode2() {
        return accountCode2;
    }

    public void setAccountCode2(String accountCode2) {
        this.accountCode2 = accountCode2;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public String getAccountCode3() {
        return accountCode3;
    }

    public void setAccountCode3(String accountCode3) {
        this.accountCode3 = accountCode3;
    }
}
