package entities.tradeLicense;

public class TradeDetails {
    private String tradeTitle;
    private String tradeType;
    private String tradeCategory;
    private String tradeSubCategory;
    private String tradeAreaWeightOfPremises;
    private String remarks;
    private String tradeCommencementDate;

    public String getTradeTitle() {
        return tradeTitle;
    }

    public void setTradeTitle(String tradeTitle) {
        this.tradeTitle = tradeTitle;
    }

    public String gettradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public String getTradeCategory() {
        return tradeCategory;
    }

    public void setTradeCategory(String tradeCategory) {
        this.tradeCategory = tradeCategory;
    }

    public String gettradeSubCategory() {
        return tradeSubCategory;
    }

    public void setTradeSubCategory(String tradeSubCategory) {
        this.tradeSubCategory = tradeSubCategory;
    }

    public String gettradeAreaWeightOfPremises() {
        return tradeAreaWeightOfPremises;
    }

    public void setTradeAreaWeightOfPremises(String tradeAreaWeightOfPremises) {
        this.tradeAreaWeightOfPremises = tradeAreaWeightOfPremises;
    }

    public String getremarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String gettradeCommencementDate() {
        return tradeCommencementDate;
    }

    public void setTradeCommencementDate(String tradeCommencementDate) {
        this.tradeCommencementDate = tradeCommencementDate;
    }
}
