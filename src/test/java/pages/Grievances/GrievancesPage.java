package pages.Grievances;

import com.jayway.awaitility.core.ConditionTimeoutException;
import entities.grievances.CreateComplaintDetails;
import org.apache.commons.lang.SystemUtils;
import org.apache.commons.collections.bag.SynchronizedSortedBag;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import pages.BasePage;

import java.util.*;
import java.util.concurrent.TimeUnit;

import static com.jayway.awaitility.Awaitility.await;
import static java.util.concurrent.TimeUnit.SECONDS;


public class GrievancesPage extends BasePage {
    private WebDriver webDriver;

    @FindBy(xpath = "html/body/div[1]/div/div[1]/div[2]/div[2]/a/div[2]")
    private WebElement registerComplaintLink;

    @FindBy(id = "f-name")
    private WebElement citizenNameTextBox;

    @FindBy(id = "mob-no")
    private WebElement mobNoTextBox;

    @FindBy(id = "email")
    private WebElement emailIdTextBox;

    @FindBy(id = "complaintTypeCategory")
    private WebElement complaintTypeCategorySelect;

    @FindBy(id = "complaintType")
    private WebElement complaintTypeSelect;

    @FindBy(id = "complaintTypeName")
    private WebElement complaintTypeTextBox;

    @FindBy(id = "doc")
    private WebElement grievanceDetailsText;

    @FindBy(id = "location")
    private WebElement grievanceLocationText;

    @FindBy(id = "landmarkDetails")
    private WebElement locationLandmarkText;

    @FindBy(css = "button[class='btn btn-primary']")
    private WebElement createGrievanceButton;

    @FindBy(xpath = ".//*[@id='main']/div[1]/div/div/div[1]/div/strong")
    private WebElement successMsg;

    @FindBy(linkText = "Grievance Redressal")
    private WebElement newRequestLink;

    @FindBy(linkText = "Register Grievance")
    private WebElement registerComplaint;

    @FindBy(linkText = "View Grievance")
    private WebElement viewGrievance;

    @FindBy(xpath = ".//*[@class='panel-title text-center no-float']")
    private WebElement CRNNumber;

    @FindBy(id = "status")
    private WebElement selectStatus;

    @FindBy(id = "inc_messge")
    private WebElement incMessageBox;

    @FindBy(xpath = "html/body/div[1]/div/div[1]/div/div/div[1]/div/strong")
    private WebElement acknMsg;

    @FindBy(linkText = "Close")
    private WebElement closeButton;

    @FindBy(id = "receivingMode1")
    private WebElement receivingModeRadio;

    @FindBy(xpath = "html/body/div[3]/header/div/ul/li[2]/a")
    private WebElement draftButton;

    @FindBy(id = "status")
    private WebElement statusSelect;

    @FindBy(xpath = ".//*[@id='inbox-template']/div[1]/div[1]/input")
    private WebElement searchCitizenInbox;

    @FindBy(xpath = ".//*[@id='tabelPortal']/tbody[2]/tr[1]/td[2]")
    private WebElement complaintLink;

    @FindBy(css = "button[type=submit]")
    private WebElement submitButton;

    @FindBy(id = "ct-location")
    private WebElement locationTextBox;

    @FindBy(id = "searchComplaints")
    private WebElement searchComplaints;

    @FindBy(id = "ct-ctno")
    private WebElement searchByAppNumTextBox;

    @FindBy(id = "when_date")
    private WebElement searchComplaintByDateBox;

    @FindBy(id = "toggle-searchcomp")
    private WebElement advanceSearchButton;

    @FindBy(id = "closeComplaints")
    private WebElement closeButton1;

    @FindBy(id = "complaintTypes")
    private WebElement bulkComplaintType;

    @FindBy(id = "boundaryType")
    private WebElement boundaryType;

    @FindBy(id = "boundaries")
    private WebElement boundaryValue;

    @FindBy(id = "position")
    private WebElement selectPosition;

    @FindBy(id = "routerSearch")
    private WebElement searchButton;

    @FindBy(id = "routersave")
    private WebElement saveButton;

    @FindBy(xpath = ".//*[@class= 'modal-footer']/button[@class='btn btn-primary']")
    private WebElement okButton;

    @FindBy(xpath = ".//*[@class='row']/div[@class='text-center']/a[@class='btn btn-default']")
    private WebElement close;

    @FindBy(id = "submitbtn")
    private WebElement submit;

    public GrievancesPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public void openCreateGrievancePage() {
        clickOnButton(registerComplaintLink, webDriver);
        switchToNewlyOpenedWindow(webDriver);
    }

    public void enterCitizenContactDetails(CreateComplaintDetails createComplaintDetails) {
        clickOnButton(receivingModeRadio, webDriver);
        enterText(citizenNameTextBox, createComplaintDetails.getCitizenName(), webDriver);
        enterText(mobNoTextBox, createComplaintDetails.getcitizenMobNo(), webDriver);
//        enterText(emailIdTextBox, createComplaintDetails.getEmailId(), webDriver);
    }

    public String enterGrievanceDetails(CreateComplaintDetails createComplaintDetails, String user) {
        if (user.equals("citizen")) {
            try{
            enterText(complaintTypeTextBox, createComplaintDetails.getGrievanceType(), webDriver);
            await().atMost(40, TimeUnit.SECONDS).until(() -> webDriver.findElements(By.cssSelector("[class='tt-dropdown-menu'] div span div p strong")).size() == 1);
            WebElement dropdown = webDriver.findElement(By.cssSelector("[class='tt-dropdown-menu'] div span div p strong"));
            dropdown.click();
            } catch (ConditionTimeoutException e){
                enterText(complaintTypeTextBox, createComplaintDetails.getGrievanceType(), webDriver);
                await().atMost(40, TimeUnit.SECONDS).until(() -> webDriver.findElements(By.cssSelector("[class='tt-dropdown-menu'] div span div p strong")).size() == 1);
                WebElement dropdown = webDriver.findElement(By.cssSelector("[class='tt-dropdown-menu'] div span div p strong"));
                dropdown.click();
            }

        } else {
            selectFromDropDown(complaintTypeCategorySelect, createComplaintDetails.getGrievanceCategory(), webDriver);
            selectFromDropDown(complaintTypeSelect, createComplaintDetails.getGrievanceType(), webDriver);
        }
        enterText(grievanceDetailsText, createComplaintDetails.getGrievanceDetails(), webDriver);
        try {
            enterText(grievanceLocationText, "a", webDriver);
            await().atMost(10, TimeUnit.SECONDS).until(() -> webDriver.findElements(By.cssSelector("[class='tt-dropdown-menu'] div span div p strong")).size() > 0);
            WebElement dropdown = webDriver.findElement(By.cssSelector("[class='tt-dropdown-menu'] div span div p strong"));
            dropdown.click();
        } catch (ConditionTimeoutException e){
            enterText(grievanceLocationText, "a", webDriver);
            await().atMost(10, TimeUnit.SECONDS).until(() -> webDriver.findElements(By.cssSelector("[class='tt-dropdown-menu'] div span div p strong")).size() > 0);
            WebElement dropdown = webDriver.findElement(By.cssSelector("[class='tt-dropdown-menu'] div span div p strong"));
            dropdown.click();
        }
        enterText(locationLandmarkText, createComplaintDetails.getLocationLandmark(), webDriver);
        clickOnButton(createGrievanceButton, webDriver);
        return successMsg.getText();
    }

//    public void getRegisterComplaintPage() {
//        clickOnButton(newRequestLink, webDriver);
//        clickOnButton(registerComplaint, webDriver);
//        switchToNewlyOpenedWindow(webDriver);
//    }

    public String getCRN() {
        String msg = CRNNumber.getText();
        String CrnNum = msg.substring(msg.lastIndexOf(": ") + 2);
        System.out.println("CRN = " + CrnNum);
        clickOnButton(closeButton, webDriver);
        switchToPreviouslyOpenedWindow(webDriver);
        return CrnNum;
    }


    public String officialMarkStatus(String status) {
        selectFromDropDown(selectStatus, status, webDriver);
        enterText(incMessageBox, status, webDriver);
        clickOnButton(submitButton, webDriver);
        String success = webDriver.findElement(By.xpath(".//*[@id='main']/div[1]/div/div/div[1]/div/strong")).getText();
        clickOnButton(closeButton, webDriver);
        switchToPreviouslyOpenedWindow(webDriver);
        return success;
    }

    public void getProcessingStatus() {
        selectFromDropDown(statusSelect, "PROCESSING", webDriver);
    }

    public void searchInCitizenInbox(String crn) {
        webDriver.navigate().refresh();
        webDriver.findElement(By.xpath(".//*[@id='totalServicesAppliedDiv']/div/div[2]")).click();
//        enterText(searchCitizenInbox, crn, webDriver);
        clickOnButton(complaintLink, webDriver);
        switchToNewlyOpenedWindow(webDriver);
    }

    public void withdrawComplaint(String complaintStatus) {
//        selectFromDropDown(selectStatus, complaintStatus, webDriver);
        enterText(incMessageBox, complaintStatus, webDriver);
        webDriver.findElement(By.cssSelector("button[class='btn btn-primary'][type='submit'")).click();
        clickOnButton(closeButton, webDriver);
        switchToPreviouslyOpenedWindow(webDriver);
    }

    public void searchComplaint(String applicationNumber, String searchType) {
        webDriver.navigate().refresh();
        switch (searchType) {

            case "appNum":
                enterText(searchByAppNumTextBox, applicationNumber, webDriver);
                break;

            case "location":
                enterText(locationTextBox, "Election Ward No. 44", webDriver);
                break;

            case "today":
                selectFromDropDown(searchComplaintByDateBox, "Today", webDriver);
                break;

            case "allDates":
                selectFromDropDown(searchComplaintByDateBox, "All", webDriver);
                break;

            case "last7Days":
                selectFromDropDown(searchComplaintByDateBox, "In Last 7 days", webDriver);
                break;

            case "last30Days":
                selectFromDropDown(searchComplaintByDateBox, "In Last 30 days", webDriver);
                break;

            case "last90Days":
                selectFromDropDown(searchComplaintByDateBox, "In Last 90 days", webDriver);
                break;

            case "status":
                clickOnButton(advanceSearchButton, webDriver);
                selectFromDropDown(webDriver.findElement(By.name("complaintStatus")), "REGISTERED", webDriver);
                break;

        }
        clickOnButton(searchComplaints, webDriver);
//        clickOnButton(webDriver.findElement(By.cssSelector("button[type='reset']")),webDriver);
//        selectFromDropDown(webDriver.findElement(By.name("complaintDate")), "All", webDriver);
//        clickOnButton(searchComplaints, webDriver);
//        List<WebElement> numOfComplaints = webDriver.findElements(By.className("sorting_1"));
//        int complaintRow = checkComplaint(numOfComplaints, applicationNumber);
//        numOfComplaints.get(complaintRow).click();
        String e = "//*[text()='" + applicationNumber + "']";
        await().atMost(10, TimeUnit.SECONDS).until(() -> webDriver.findElements(By.xpath(e)).size() == 1);
        webDriver.findElement(By.xpath(e)).click();
        switchToNewlyOpenedWindow(webDriver);
        clickOnButton(webDriver.findElement(By.cssSelector("[type='button'][onclick]")), webDriver);
        List<String> webPages = new ArrayList<>(webDriver.getWindowHandles());
        webDriver.switchTo().window(webPages.get(1));
    }

    private int checkComplaint(List<WebElement> numOfComplaints, String applicationNumber) {
        int rowNumber = 0;
        boolean found = false;
        for (int i = 0; i < numOfComplaints.size(); i++) {
            if (webDriver.findElements(By.className("sorting_1")).get(i).getText().contains(applicationNumber)) {
                rowNumber = i;
                found = true;
            }
        }
        if (found)
            return rowNumber;
        else
            throw new RuntimeException("Element Not Found");

    }

    public void close() {
        clickOnButton(closeButton1, webDriver);
        switchToPreviouslyOpenedWindow(webDriver);
    }

    public String searchComplaint(String applicationNumber) {
        enterText(searchByAppNumTextBox, applicationNumber, webDriver);
        clickOnButton(searchComplaints, webDriver);
        webDriver.findElement(By.cssSelector("td[class ='sorting_1']")).click();
        switchToNewlyOpenedWindow(webDriver);
        waitForElementToBeVisible(webDriver.findElement(By.xpath(".//*[@class='row  add-border']/div[4]")), webDriver);
        String str = webDriver.findElement(By.xpath(".//*[@class='row  add-border']/div[4]")).getText();
        String arr[] = str.split(":");
        String user = arr[0];
        webDriver.close();
        switchToNewlyOpenedWindow(webDriver);
        webDriver.close();
        switchToPreviouslyOpenedWindow(webDriver);
        return user;
    }

    public void getSearchComplaintPage() {
        clickOnButton(newRequestLink, webDriver);
        webDriver.manage().window().maximize();
        clickOnButton(viewGrievance, webDriver);
        switchToNewlyOpenedWindow(webDriver);
    }

    public void bulkRouter(CreateComplaintDetails createComplaintDetails) {
        selectFromDropDown(complaintTypeCategorySelect, createComplaintDetails.getGrievanceCategory(), webDriver);
        selectFromDropDown(bulkComplaintType, createComplaintDetails.getGrievanceType(), webDriver);
        selectFromDropDown(boundaryType, createComplaintDetails.getBoundary(), webDriver);

        Select sel = new Select(webDriver.findElement(By.id("boundaries")));
        List<WebElement> boundary = sel.getOptions();
        webDriver.findElement(By.id("boundaries")).sendKeys(Keys.CONTROL);
        for (WebElement webElement : boundary) {
            webElement.click();
        }
        enterText(selectPosition, "TLSSOne", webDriver);

        await().atMost(20, TimeUnit.SECONDS).until(() -> webDriver.findElements(By.cssSelector("[class='tt-dropdown-menu'] div span div p strong")).size() == 1);

        WebElement dropdown = webDriver.findElement(By.cssSelector("[class='tt-dropdown-menu'] div span div p strong"));
        dropdown.click();
        clickOnButton(searchButton, webDriver);
        clickOnButton(saveButton, webDriver);
        if (isWindows()) {
            waitForElementToBeVisible(okButton, webDriver);
            clickOnButton(okButton, webDriver);
        }
        clickOnButton(close, webDriver);
        switchToPreviouslyOpenedWindow(webDriver);
        webDriver.navigate().refresh();
    }


    public void serviceCompleted() {
        webDriver.navigate().refresh();
        webDriver.findElement(By.xpath(".//*[@id='servicesCmpletedDiv']/div/div[2]")).click();
        webDriver.findElement(By.xpath(".//*[@class='totalServicesCompletedHide']/tr[1]")).click();
        switchToNewlyOpenedWindow(webDriver);

    }

    public void feedbackRating() {
        enterText(incMessageBox, "Good work", webDriver);
        webDriver.findElement(By.xpath(".//*[@class='form-group'][2]/div[2]/span/div[@class='rating-symbol'][4]")).click();
        clickOnButton(submit, webDriver);
        clickOnButton(closeButton, webDriver);
        switchToPreviouslyOpenedWindow(webDriver);
        webDriver.navigate().refresh();

    }

    public void crn(String applicationNumber, String appNum) {
        enterText(searchByAppNumTextBox, applicationNumber, webDriver);
        clickOnButton(searchComplaints, webDriver);
        webDriver.findElement(By.cssSelector("td[class ='sorting_1']")).click();
        switchToNewlyOpenedWindow(webDriver);
        WebElement ratings = webDriver.findElement(By.xpath(".//*[@class='rating-symbol-background fa fa-star-o fa-2x']"));
        List<WebElement> starRating = webDriver.findElements(By.xpath(".//*[@class='fa fa-star fa-2x symbol-filled']"));
        Iterator<WebElement> iterator = starRating.iterator();
        int i = 0;
        while (iterator.hasNext()) {
            iterator.next();
            i++;
        }

        System.out.println("Rating = " + i);
        webDriver.findElement(By.xpath(".//*[@class='text-center']/button[@class='btn btn-default']")).click();
        List<String> webPages = new ArrayList<>(webDriver.getWindowHandles());
        webDriver.switchTo().window(webPages.get(1));
        clickOnButton(closeButton1, webDriver);
        switchToPreviouslyOpenedWindow(webDriver);
        webDriver.navigate().refresh();


    }

    private boolean isWindows() {
        return SystemUtils.IS_OS_WINDOWS;
    }

    private boolean isLinux() {
        return SystemUtils.IS_OS_LINUX;
    }

    private boolean isMac() {
        return SystemUtils.IS_OS_MAC;

    }
}


