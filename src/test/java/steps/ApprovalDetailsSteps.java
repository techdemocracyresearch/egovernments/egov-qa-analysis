package steps;

import cucumber.api.java8.En;
import entities.ApprovalDetails;
import excelDataFiles.ExcelReader;
import pages.ApprovalDetailsPage;

public class ApprovalDetailsSteps extends BaseSteps implements En {

    public ApprovalDetailsSteps() {

        And("^he forwards for approver (.*)$", (String approvalDetailsDataId) -> {
            ApprovalDetails approvalDetails = new ExcelReader(approvalDetailsTestDataFileName).getApprovalDetails(approvalDetailsDataId);
            if (approvalDetailsDataId.equals("sanitaryInspector1") || approvalDetailsDataId.equals("Grievanceofficer1") || approvalDetailsDataId.equals("commissioner1") || approvalDetailsDataId.equals("CM_commissioner")) {
                pageStore.get(ApprovalDetailsPage.class).enterApprovalDetails(approvalDetails);
                if (approvalDetailsDataId.equals("commissioner1") || approvalDetailsDataId.equals("CM_commissioner")) {
                    pageStore.get(ApprovalDetailsPage.class).forward();
                } else {
                    pageStore.get(ApprovalDetailsPage.class).createGrievance();
                }
            } else if (approvalDetailsDataId.equals("sanitaryInspector") || approvalDetailsDataId.equals("commissioner") || approvalDetailsDataId.equals("commissioner2") || approvalDetailsDataId.equals("TL_SI") || approvalDetailsDataId.equals("TL_Commissioner") || approvalDetailsDataId.equals("TL_AMOH") || approvalDetailsDataId.equals("TL_MHO") || approvalDetailsDataId.equals("TL_CMOH") || approvalDetailsDataId.equals("TL_SS")){
                pageStore.get(ApprovalDetailsPage.class).enterApproverDetails(approvalDetails);
                pageStore.get(ApprovalDetailsPage.class).forward();
            }
        });

    }
}
