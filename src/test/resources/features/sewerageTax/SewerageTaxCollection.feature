Feature: Create/renewal/closure of sewerage connection
  As a registered user of the system
  User should be able to create/change/closure of sewerage connection

  Background:It will run the data entry screen of property tax
    Given PTISCommissioner logs in
    And user will select the required screen as "Data entry screen" with condition as "ptis"
    And he creates a new assessment for a private residential property
    Then dataEntry Details saved successfully
    And he choose to add edit DCB
    And he choose to close the dataentry acknowledgement screen
    And current user logs out

  @SewerageTax @Sanity @stms
  Scenario Outline: create change closure of sewerage connection

    Given SewerageJuniorAssistant logs in
    And user will select the required screen as "Property Tax"
    And he chooses to collect tax for above assessment number
#    And he chooses to pay tax
    And he collect tax using <paymentMode>
    And user closes the acknowledgement
    And current user logs out

    When CSCUser logs in
    And user will select the required screen as "Apply for new connection" with condition as "stms"
    And he create new sewerage connection for above assessment number <creationDocuments>
    And he forward to junior assistant and closes the acknowledgement
    And current user logs out

    When SewerageJuniorAssistant logs in
    And he chooses to act upon above application number
    And he forward to AE and close the acknowledgement
    Then user will be notified by "forwarded"
    And current user logs out

    When SewerageAssistantEngineer logs in
    And he chooses to act upon above application number
    And he enter Pipe Details
    And he enter Estimation Details
    And he forward to DEE and close the acknowledgement
    Then user will be notified by "forwarded"
    And current user logs out

    When SewerageDeputyExecutiveEngineer logs in
    And he chooses to act upon above application number
    And he forward to executive engineer and closes the acknowledgement
    Then user will be notified by "successfully"
    And current user logs out

    When SewerageExecutiveEngineer logs in
    And he chooses to act upon above application number
    And he generates estimation notice for above sewerage application
    And current user logs out

    When SewerageJuniorAssistant logs in
    And user will select the required screen as "Collect Sewerage Charges"
    And he search for above application number to collect
    And he collect the charges and closes the acknowledgement
    And current user logs out


    When SewerageExecutiveEngineer logs in
    And he chooses to act upon above application number
    And he approve the above sewerage application
    And he closes the sewerage acknowledgement
    Then user will be notified by "successfully"
    And current user logs out

    When SewerageAssistantEngineer logs in
    And user will select the required screen as "Execute Sewerage Connection"
    And search for above sewerage application
    And he execute connection and closes the acknowledgement
#    Then user will be notified by "completed"
    And current user logs out

     ########################################################
     #############  change of sewerage connection ###########
     #########################################################

    Given SewerageJuniorAssistant logs in
    And user will select the required screen as "search connection" with condition as "stms"
    And he search for above sewerage connection
    And he increases the number of closets <changeDocuments>
    And he forward to assistant engineer for change in closets and closes the acknowledgement
    Then user will be notified by "forwarded"
    And current user logs out

    When SewerageAssistantEngineer logs in
    And he chooses to act upon above application number
    And he enter Pipe Details
    And he enter Estimation Details
    And he forward to DEE for change in closets and close the acknowledgement
    Then user will be notified by "forwarded"
    And current user logs out

    When SewerageDeputyExecutiveEngineer logs in
    And he chooses to act upon above application number
    And he forward to executive engineer for change in closets and closes the acknowledgement
    Then user will be notified by "successfully"
    And current user logs out

    When SewerageExecutiveEngineer logs in
    And he chooses to act upon above application number
    And he generates estimation notice for above sewerage application
    And current user logs out

    When SewerageJuniorAssistant logs in
    And user will select the required screen as "Collect Sewerage Charges"
    And he search for above application number to collect
    And he collect the charges and closes the acknowledgement
    And current user logs out


    When SewerageExecutiveEngineer logs in
    And he chooses to act upon above application number
    And he approve the above sewerage application
    And he closes the sewerage acknowledgement
    Then user will be notified by "successfully"
    And current user logs out


    When SewerageAssistantEngineer logs in
    And user will select the required screen as "Execute Sewerage Connection"
    And search for above sewerage application
    And he execute connection and closes the acknowledgement
    And current user logs out

     #########################################################
     #############  closure of sewerage connection ###########
     #########################################################

    Given SewerageJuniorAssistant logs in
    And user will select the required screen as "search connection" with condition as "stms"
    And he search for above sewerage application for closure
    And he put remarks and forward the application
    And he forwards for closure and closes the acknowledgement
    Then user will be notified by "forwarded"
    And current user logs out

    When SewerageAssistantEngineer logs in
    And he chooses to act upon above application number
    And he forwards to DEE for closure and close the acknowledgement
    Then user will be notified by "forwarded"
    And current user logs out

    When SewerageDeputyExecutiveEngineer logs in
    And he chooses to act upon above application number
    And he forwards to executive engineer for closure and close the acknowledgement
    Then user will be notified by "forwarded"
    And current user logs out

    When SewerageExecutiveEngineer logs in
    And he chooses to act upon above application number
    And he approve the above sewerage application
    And he closes the seweargeClosure acknowledgement
    And current user logs out

    Examples:
      | paymentMode | creationDocuments |Pipe Details| changeDocuments |
      | cash        | creation1         | pipeDetails |change1         |
#      | cash        | creation2         | change2         |
#      | cash        | creation3         | change3         |

  @SewerageTax @Sanity
  Scenario: Generate demand bill for legacy sewerage connection

    Given PTISCommissioner logs in
    And user will select the required screen as "Data Entry Screen" with condition as "stms"
    And he enter details for legacy sewerage connection
    And he submit the application of legacy sewerage connection and closes the acknowledgement
    Then user will be notified by "successfully."
    And current user logs out

    When SewerageJuniorAssistant logs in
    And user will select the required screen as "search connection" with condition as "stms"
    And he search application and generate demand bill
    And current user logs out