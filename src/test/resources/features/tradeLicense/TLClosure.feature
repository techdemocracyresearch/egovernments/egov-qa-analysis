Feature: Trade license closure

# Trade License Closure #

@Sanity @TradeLicense2
Scenario Outline: Registered user choose for trade license closure(Approved by Commissioner)

  Given CSCUser logs in
  And user will select the required screen as "Search Trade License"
  And he choose a trade license for closure as <closureDetails>
  And he choose to close trade license
  And he closes search screen
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Acknowledged"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SI1
  And he confirms to proceed
  And he closes the acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "UnderWorkflow"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SS1
  And he confirms to proceed
  And he closes the acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_SS logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_Commissioner1
  And he confirms to proceed
  And he closes the acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_ADM_Commissioner logs in
  And he chooses to act upon above application number
  And he approves the closure
  And he confirms to proceed
  # And he generate Endorsement notice #
  And he closes the acknowledgement page
  And he verifies that application not in his inbox
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Cancelled"
  And he verifies the License active
  And user will be notified by "No"
  And he closes search screen
  And current user logs out

  Examples:
  | closureDetails    |
  | licenceForClosure |

@Sanity @TradeLicense
Scenario Outline: Registered user choose for trade license closure(Approved by AMOH)

  Given CSCUser logs in
  And user will select the required screen as "Search Trade License"
  And he choose a trade license for closure as <closureDetails>
  And he choose to close trade license
  And he closes search screen
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Acknowledged"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SI1
  And he confirms to proceed
  And he closes the acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "UnderWorkflow"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SS1
  And he confirms to proceed
  And he closes the acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_SS logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_AMOH1
  And he confirms to proceed
  And he closes the acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_AMOH logs in
  And he chooses to act upon above application number
  And he approves the closure
  And he confirms to proceed
  # And he generate Endorsement notice #
  And he closes the acknowledgement page
  And he verifies that application not in his inbox
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Cancelled"
  And he verifies the License active
  And user will be notified by "No"
  And he closes search screen
  And current user logs out

  Examples:
  | closureDetails    |
  | licenceForClosure |

@Sanity @TradeLicense
Scenario Outline: Registered user choose for trade license closure(Approved by MHO)

  Given CSCUser logs in
  And user will select the required screen as "Search Trade License"
  And he choose a trade license for closure as <closureDetails>
  And he choose to close trade license
  And he closes search screen
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Acknowledged"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SI1
  And he confirms to proceed
  And he closes the acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "UnderWorkflow"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SS1
  And he confirms to proceed
  And he closes the acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_SS logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_MHO1
  And he confirms to proceed
  And he closes the acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_MHO logs in
  And he chooses to act upon above application number
  And he approves the closure
  And he confirms to proceed
  # And he generate Endorsement notice #
  And he closes the acknowledgement page
  And he verifies that application not in his inbox
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Cancelled"
  And he verifies the License active
  And user will be notified by "No"
  And he closes search screen
  And current user logs out

  Examples:
  | closureDetails    |
  | licenceForClosure |

@Sanity @TradeLicense
Scenario Outline: Registered user choose for trade license closure(Approved by CMOH)

  Given CSCUser logs in
  And user will select the required screen as "Search Trade License"
  And he choose a trade license for closure as <closureDetails>
  And he choose to close trade license
  And he closes search screen
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Acknowledged"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SI1
  And he confirms to proceed
  And he closes the acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "UnderWorkflow"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SS1
  And he confirms to proceed
  And he closes the acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_SS logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_CMOH1
  And he confirms to proceed
  And he closes the acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_CMOH logs in
  And he chooses to act upon above application number
  And he approves the closure
  And he confirms to proceed
  # And he generate Endorsement notice #
  And he closes the acknowledgement page
  And he verifies that application not in his inbox
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Cancelled"
  And he verifies the License active
  And user will be notified by "No"
  And he closes search screen
  And current user logs out

  Examples:
  | closureDetails    |
  | licenceForClosure |


@Sanity @TradeLicense
Scenario Outline: Registered user choose for trade license closure and commissioner rejects it

  Given CSCUser logs in
  And user will select the required screen as "Search Trade License"
  And he choose a trade license for closure as <closureDetails>
  And he choose to close trade license
  And he closes search screen
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Acknowledged"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SI1
  And he confirms to proceed
  And he closes the acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "UnderWorkflow"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SS1
  And he confirms to proceed
  And he closes the acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_SS logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_Commissioner1
  And he confirms to proceed
  And he closes the acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_ADM_Commissioner logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes the acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes the acknowledgement page
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he cancel the application
  And he confirms to proceed
  And he closes the acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Active"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And current user logs out

  Examples:
  | closureDetails    |
  | licenceForClosure |

@Sanity @TradeLicense
Scenario Outline: Registered user choose for trade license closure and AMOH rejects it

  Given CSCUser logs in
  And user will select the required screen as "Search Trade License"
  And he choose a trade license for closure as <closureDetails>
  And he choose to close trade license
  And he closes search screen
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Acknowledged"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SI1
  And he confirms to proceed
  And he closes the acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "UnderWorkflow"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SS1
  And he confirms to proceed
  And he closes the acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_SS logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_AMOH1
  And he confirms to proceed
  And he closes the acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_AMOH logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes the acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes the acknowledgement page
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he cancel the application
  And he confirms to proceed
  And he closes the acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Active"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And current user logs out

  Examples:
  | closureDetails    |
  | licenceForClosure |

@Sanity @TradeLicense
Scenario Outline: Registered user choose for trade license closure and MHO rejects it

  Given CSCUser logs in
  And user will select the required screen as "Search Trade License"
  And he choose a trade license for closure as <closureDetails>
  And he choose to close trade license
  And he closes search screen
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Acknowledged"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SI1
  And he confirms to proceed
  And he closes the acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "UnderWorkflow"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SS1
  And he confirms to proceed
  And he closes the acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_SS logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_MHO1
  And he confirms to proceed
  And he closes the acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_MHO logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes the acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes the acknowledgement page
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he cancel the application
  And he confirms to proceed
  And he closes the acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Active"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And current user logs out

  Examples:
  | closureDetails    |
  | licenceForClosure |

@Sanity @TradeLicense
Scenario Outline: Registered user choose for trade license closure and CMOH rejects it

  Given CSCUser logs in
  And user will select the required screen as "Search Trade License"
  And he choose a trade license for closure as <closureDetails>
  And he choose to close trade license
  And he closes search screen
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Acknowledged"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SI1
  And he confirms to proceed
  And he closes the acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "UnderWorkflow"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SS1
  And he confirms to proceed
  And he closes the acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_SS logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_CMOH1
  And he confirms to proceed
  And he closes the acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_CMOH logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes the acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes the acknowledgement page
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he cancel the application
  And he confirms to proceed
  And he closes the acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Active"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And current user logs out

  Examples:
    | closureDetails    |
    | licenceForClosure |

@Sanity @TradeLicense
Scenario Outline: Registered user choose for trade license closure and SS rejects it

  Given CSCUser logs in
  And user will select the required screen as "Search Trade License"
  And he choose a trade license for closure as <closureDetails>
  And he choose to close trade license
  And he closes search screen
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Acknowledged"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SI1
  And he confirms to proceed
  And he closes the acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "UnderWorkflow"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SS1
  And he confirms to proceed
  And he closes the acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_SS logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes the acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes the acknowledgement page
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he cancel the application
  And he confirms to proceed
  And he closes the acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Active"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And current user logs out

  Examples:
  | closureDetails    |
  | licenceForClosure |

@Sanity @TradeLicense
Scenario Outline: Registered user choose for trade license closure and SI rejects it

  Given CSCUser logs in
  And user will select the required screen as "Search Trade License"
  And he choose a trade license for closure as <closureDetails>
  And he choose to close trade license
  And he closes search screen
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Acknowledged"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SI1
  And he confirms to proceed
  And he closes the acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "UnderWorkflow"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes the acknowledgement page
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he cancel the application
  And he confirms to proceed
  And he closes the acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Active"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And current user logs out

  Examples:
  | closureDetails    |
  | licenceForClosure |

@Sanity @TradeLicense
Scenario Outline:  Registered user choose for trade license closure and JA cancel it

  Given CSCUser logs in
  And user will select the required screen as "Search Trade License"
  And he choose a trade license for closure as <closureDetails>
  And he choose to close trade license
  And he closes search screen
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Acknowledged"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he cancel the application
  And he confirms to proceed
  And he closes the acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Active"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And current user logs out

  Examples:
  | closureDetails    |
  | licenceForClosure |





