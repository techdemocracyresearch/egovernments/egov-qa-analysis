Feature: Create New License Rejections

@Sanity @TradeLicense
Scenario Outline: Create new TL from CSCUSER-> collect fee -> forward to SI -> Change trade area and forward to Commissioner
-> Approve in commissioner -> reject

  Given CSCUser logs in
  And user will select the required screen as "Create New License"
  And he enters trade owner details of new license <tradeDetailsData>
  And he enters trade location details of new license <tradeLocationData>
  And he enters trade details of new license <tradeDetailsData1>
  And he saves the application
  And he confirms to proceed
  And he copy trade application number
  And current user logs out

  When TL_PHS_JA logs in
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber
  And he chooses to act upon above application number

  And he forwards for TL approver TL_SI
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he changes trade area as "1200"
  And he forwards for TL approver TL_Commissioner
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_ADM_Commissioner logs in
  And he chooses to act upon above application number
  And he approves application
  And he confirms to proceed
  And he closes acknowledgement page
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he cancel the application
  And he confirms to proceed
  And he closes acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he verifies the application status
  And user will be notified by "Cancelled"
  And he verifies the License active
  And user will be notified by "No"
  And he closes search screen
  And current user logs out

  Examples:
  | tradeDetailsData         | tradeLocationData           | tradeDetailsData1        |
  | ownerDetailsTradeLicense | locationDetailsTradeLicense | tradeDetailsTradeLicense |

@Sanity @TradeLicense
Scenario Outline: Create new TL from CSCUSER -> collect fee -> forward to SI-> forward to Commissioner -> reject

  Given CSCUser logs in
  And user will select the required screen as "Create New License"
  And he enters trade owner details of new license <tradeDetailsData>
  And he enters trade location details of new license <tradeLocationData>
  And he enters trade details of new license <tradeDetailsData1>
  And he saves the application
  And he confirms to proceed
  And he copy trade application number
  And current user logs out

  When TL_PHS_JA logs in
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber
  And he chooses to act upon above application number

  And he forwards for TL approver TL_SI
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_Commissioner
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_ADM_Commissioner logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he cancel the application
  And he confirms to proceed
  And he closes acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he verifies the application status
  And user will be notified by "Cancelled"
  And he verifies the License active
  And user will be notified by "No"
  And he closes search screen
  And current user logs out

  Examples:
  | tradeDetailsData         | tradeLocationData           | tradeDetailsData1        |
  | ownerDetailsTradeLicense | locationDetailsTradeLicense | tradeDetailsTradeLicense |

@Sanity @TradeLicense
Scenario Outline: Create new TL -> collect fee -> forward to SI -> reject

  Given CSCUser logs in
  And user will select the required screen as "Create New License"
  And he enters trade owner details of new license <tradeDetailsData>
  And he enters trade location details of new license <tradeLocationData>
  And he enters trade details of new license <tradeDetailsData1>
  And he saves the application
  And he confirms to proceed
  And he copy trade application number
  And current user logs out

  When TL_PHS_JA logs in
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber
  And he chooses to act upon above application number

  And he forwards for TL approver TL_SI
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he cancel the application
  And he confirms to proceed
  And he closes acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he verifies the application status
  And user will be notified by "Cancelled"
  And he verifies the License active
  And user will be notified by "No"
  And he closes search screen
  And current user logs out

  Examples:
  | tradeDetailsData         | tradeLocationData           | tradeDetailsData1        |
  | ownerDetailsTradeLicense | locationDetailsTradeLicense | tradeDetailsTradeLicense |

@Sanity @TradeLicense
Scenario Outline: Create new TL -> collect fee -> reject

  Given CSCUser logs in
  And user will select the required screen as "Create New License"
  And he enters trade owner details of new license <tradeDetailsData>
  And he enters trade location details of new license <tradeLocationData>
  And he enters trade details of new license <tradeDetailsData1>
  And he saves the application
  And he confirms to proceed
  And he copy trade application number
  And current user logs out

  When TL_PHS_JA logs in
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber
  And he chooses to act upon above application number
  And he cancel the application
  And he confirms to proceed
  And he closes acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he verifies the application status
  And user will be notified by "Cancelled"
  And he verifies the License active
  And user will be notified by "No"
  And he closes search screen
  And current user logs out

  Examples:
  | tradeDetailsData         | tradeLocationData           | tradeDetailsData1        |
  | ownerDetailsTradeLicense | locationDetailsTradeLicense | tradeDetailsTradeLicense |

@Sanity @TradeLicense
Scenario Outline: Create new TL -> reject

  Given CSCUser logs in
  And user will select the required screen as "Create New License"
  And he enters trade owner details of new license <tradeDetailsData>
  And he enters trade location details of new license <tradeLocationData>
  And he enters trade details of new license <tradeDetailsData1>
  And he saves the application
  And he confirms to proceed
  And he copy trade application number
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he cancel the application
  And he confirms to proceed
  And he closes acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he verifies the application status
  And user will be notified by "Cancelled"
  And he verifies the License active
  And user will be notified by "No"
  And he closes search screen
  And current user logs out

  Examples:
  | tradeDetailsData         | tradeLocationData           | tradeDetailsData1        |
  | ownerDetailsTradeLicense | locationDetailsTradeLicense | tradeDetailsTradeLicense |

@Sanity @TradeLicense
Scenario Outline: Create new TL from CSCUser-> collect fee -> forward to SI -> forward to SS ->reject

  Given CSCuser logs in
  And user will select the required screen as "Create New License"
  And he enters trade owner details of new license <tradeDetailsData>
  And he enters trade location details of new license <tradeLocationData>
  And he enters trade details of new license <tradeDetailsData1>
  And he saves the application
  And he confirms to proceed
  And he copy trade application number
  And current user logs out

  When TL_PHS_JA logs in
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber
  And he chooses to act upon above application number

  And he forwards for TL approver TL_SI
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SS
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SS logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he cancel the application
  And he confirms to proceed
  And he closes acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he verifies the application status
  And user will be notified by "Cancelled"
  And he verifies the License active
  And user will be notified by "No"
  And he closes search screen
  And current user logs out

  Examples:
  | tradeDetailsData         | tradeLocationData           | tradeDetailsData1        |
  | ownerDetailsTradeLicense | locationDetailsTradeLicense | tradeDetailsTradeLicense |

@Sanity @TradeLicense
Scenario Outline: Create New Trade License from CSCUSER -> Collect Fee -> Forward to SI -> Forward to SS -> Forward to AMOH -> Reject

  Given CSCUser logs in
  And user will select the required screen as "Create New License"
  And he enters trade owner details of new license <tradeDetailsData>
  And he enters trade location details of new license <tradeLocationData>
  And he enters trade details of new license <tradeDetailsData1>
  And he saves the application
  And he confirms to proceed
  And he copy trade application number
  And current user logs out

  When TL_PHS_JA logs in
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber
  And he chooses to act upon above application number

  And he forwards for TL approver TL_SI
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SS
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SS logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_AMOH
  And he confirms to proceed
  And he closes acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_AMOH logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he cancel the application
  And he confirms to proceed
  And he closes acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he verifies the application status
  And user will be notified by "Cancelled"
  And he verifies the License active
  And user will be notified by "No"
  And he closes search screen
  And current user logs out

  Examples:
  | tradeDetailsData         | tradeLocationData           | tradeDetailsData1        |
  | ownerDetailsTradeLicense | locationDetailsTradeLicense | tradeDetailsTradeLicense |

@Sanity @TradeLicense
Scenario Outline: Create New Trade License from CSCUSER -> Collect Fee -> Forward to SI ->Forward to SS -> Forward to MHO -> Reject

  Given CSCUser logs in
  And user will select the required screen as "Create New License"
  And he enters trade owner details of new license <tradeDetailsData>
  And he enters trade location details of new license <tradeLocationData>
  And he enters trade details of new license <tradeDetailsData1>
  And he saves the application
  And he confirms to proceed
  And he copy trade application number
  And current user logs out

  When TL_PHS_JA logs in
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber
  And he chooses to act upon above application number

  And he forwards for TL approver TL_SI
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SS
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SS logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_MHO
  And he confirms to proceed
  And he closes acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_MHO logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he cancel the application
  And he confirms to proceed
  And he closes acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he verifies the application status
  And user will be notified by "Cancelled"
  And he verifies the License active
  And user will be notified by "No"
  And he closes search screen
  And current user logs out

  Examples:
  | tradeDetailsData         | tradeLocationData           | tradeDetailsData1        |
  | ownerDetailsTradeLicense | locationDetailsTradeLicense | tradeDetailsTradeLicense |

@Sanity @TradeLicense
Scenario Outline: Create New Trade License from CSCUSER -> Collect Fee -> Forward to SI ->Forward to SS -> Forward to CMOH-Reject

  Given CSCUser logs in
  And user will select the required screen as "Create New License"
  And he enters trade owner details of new license <tradeDetailsData>
  And he enters trade location details of new license <tradeLocationData>
  And he enters trade details of new license <tradeDetailsData1>
  And he saves the application
  And he confirms to proceed
  And he copy trade application number
  And current user logs out

  When TL_PHS_JA logs in
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber
  And he chooses to act upon above application number

  And he forwards for TL approver TL_SI
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SS
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SS logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_CMOH
  And he confirms to proceed
  And he closes acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_CMOH logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he cancel the application
  And he confirms to proceed
  And he closes acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he verifies the application status
  And user will be notified by "Cancelled"
  And he verifies the License active
  And user will be notified by "No"
  And he closes search screen
  And current user logs out

  Examples:
  | tradeDetailsData         | tradeLocationData           | tradeDetailsData1        |
  | ownerDetailsTradeLicense | locationDetailsTradeLicense | tradeDetailsTradeLicense |

@Sanity @TradeLicense
Scenario Outline: Create new TL from CSCUSER-> collect fee -> forward to SI -> Change trade area and forward to AMOH
-> Approve in AMOH -> reject

  Given CSCUser logs in
  And user will select the required screen as "Create New License"
  And he enters trade owner details of new license <tradeDetailsData>
  And he enters trade location details of new license <tradeLocationData>
  And he enters trade details of new license <tradeDetailsData1>
  And he saves the application
  And he confirms to proceed
  And he copy trade application number
  And current user logs out

  When TL_PHS_JA logs in
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber
  And he chooses to act upon above application number

  And he forwards for TL approver TL_SI
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he changes trade area as "1200"
  And he forwards for TL approver TL_AMOH
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_AMOH logs in
  And he chooses to act upon above application number
  And he approves application
  And he confirms to proceed
  And he closes acknowledgement page
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he cancel the application
  And he confirms to proceed
  And he closes acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he verifies the application status
  And user will be notified by "Cancelled"
  And he verifies the License active
  And user will be notified by "No"
  And he closes search screen
  And current user logs out

  Examples:
  | tradeDetailsData         | tradeLocationData           | tradeDetailsData1        |
  | ownerDetailsTradeLicense | locationDetailsTradeLicense | tradeDetailsTradeLicense |

@Sanity @TradeLicense
Scenario Outline: Create new TL from CSCUSER-> collect fee -> forward to SI -> Change trade area and forward to MHO
-> Approve in MHO -> reject

  Given CSCUser logs in
  And user will select the required screen as "Create New License"
  And he enters trade owner details of new license <tradeDetailsData>
  And he enters trade location details of new license <tradeLocationData>
  And he enters trade details of new license <tradeDetailsData1>
  And he saves the application
  And he confirms to proceed
  And he copy trade application number
  And current user logs out

  When TL_PHS_JA logs in
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber
  And he chooses to act upon above application number

  And he forwards for TL approver TL_SI
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he changes trade area as "1200"
  And he forwards for TL approver TL_MHO
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_MHO logs in
  And he chooses to act upon above application number
  And he approves application
  And he confirms to proceed
  And he closes acknowledgement page
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he cancel the application
  And he confirms to proceed
  And he closes acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he verifies the application status
  And user will be notified by "Cancelled"
  And he verifies the License active
  And user will be notified by "No"
  And he closes search screen
  And current user logs out

  Examples:
  | tradeDetailsData         | tradeLocationData           | tradeDetailsData1        |
  | ownerDetailsTradeLicense | locationDetailsTradeLicense | tradeDetailsTradeLicense |

@Sanity @TradeLicense
Scenario Outline: Create new TL from CSCUSER-> collect fee -> forward to SI -> Change trade area and forward to CMOH
-> Approve in CMOH -> reject

  Given CSCUser logs in
  And user will select the required screen as "Create New License"
  And he enters trade owner details of new license <tradeDetailsData>
  And he enters trade location details of new license <tradeLocationData>
  And he enters trade details of new license <tradeDetailsData1>
  And he saves the application
  And he confirms to proceed
  And he copy trade application number
  And current user logs out

  When TL_PHS_JA logs in
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber
  And he chooses to act upon above application number

  And he forwards for TL approver TL_SI
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he changes trade area as "1200"
  And he forwards for TL approver TL_CMOH
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_CMOH logs in
  And he chooses to act upon above application number
  And he approves application
  And he confirms to proceed
  And he closes acknowledgement page
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he cancel the application
  And he confirms to proceed
  And he closes acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he verifies the application status
  And user will be notified by "Cancelled"
  And he verifies the License active
  And user will be notified by "No"
  And he closes search screen
  And current user logs out

  Examples:
  | tradeDetailsData         | tradeLocationData           | tradeDetailsData1        |
  | ownerDetailsTradeLicense | locationDetailsTradeLicense | tradeDetailsTradeLicense |

@Sanity @TradeLicense
Scenario Outline: Create new TL from CSCUSER-> collect fee -> forward to SI -> Change trade area and forward to Commissioner
-> reject

  Given CSCUser logs in
  And user will select the required screen as "Create New License"
  And he enters trade owner details of new license <tradeDetailsData>
  And he enters trade location details of new license <tradeLocationData>
  And he enters trade details of new license <tradeDetailsData1>
  And he saves the application
  And he confirms to proceed
  And he copy trade application number
  And current user logs out

  When TL_PHS_JA logs in
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber
  And he chooses to act upon above application number

  And he forwards for TL approver TL_SI
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he changes trade area as "1200"
  And he forwards for TL approver TL_Commissioner
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_ADM_Commissioner logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he cancel the application
  And he confirms to proceed
  And he closes acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he verifies the application status
  And user will be notified by "Cancelled"
  And he verifies the License active
  And user will be notified by "No"
  And he closes search screen
  And current user logs out

  Examples:
  | tradeDetailsData         | tradeLocationData           | tradeDetailsData1        |
  | ownerDetailsTradeLicense | locationDetailsTradeLicense | tradeDetailsTradeLicense |

@Sanity @TradeLicense
Scenario Outline: Create new TL from CSCUSER-> collect fee -> forward to SI -> Change trade area and forward to AMOH
-> reject

  Given CSCUser logs in
  And user will select the required screen as "Create New License"
  And he enters trade owner details of new license <tradeDetailsData>
  And he enters trade location details of new license <tradeLocationData>
  And he enters trade details of new license <tradeDetailsData1>
  And he saves the application
  And he confirms to proceed
  And he copy trade application number
  And current user logs out

  When TL_PHS_JA logs in
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber
  And he chooses to act upon above application number

  And he forwards for TL approver TL_SI
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he changes trade area as "1200"
  And he forwards for TL approver TL_AMOH
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_AMOH logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he cancel the application
  And he confirms to proceed
  And he closes acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he verifies the application status
  And user will be notified by "Cancelled"
  And he verifies the License active
  And user will be notified by "No"
  And he closes search screen
  And current user logs out

  Examples:
  | tradeDetailsData         | tradeLocationData           | tradeDetailsData1        |
  | ownerDetailsTradeLicense | locationDetailsTradeLicense | tradeDetailsTradeLicense |

@Sanity @TradeLicense
Scenario Outline: Create new TL from CSCUSER-> collect fee -> forward to SI -> Change trade area and forward to MHO
-> reject

  Given CSCUser logs in
  And user will select the required screen as "Create New License"
  And he enters trade owner details of new license <tradeDetailsData>
  And he enters trade location details of new license <tradeLocationData>
  And he enters trade details of new license <tradeDetailsData1>
  And he saves the application
  And he confirms to proceed
  And he copy trade application number
  And current user logs out

  When TL_PHS_JA logs in
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber
  And he chooses to act upon above application number

  And he forwards for TL approver TL_SI
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he changes trade area as "1200"
  And he forwards for TL approver TL_MHO
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_MHO logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he cancel the application
  And he confirms to proceed
  And he closes acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he verifies the application status
  And user will be notified by "Cancelled"
  And he verifies the License active
  And user will be notified by "No"
  And he closes search screen
  And current user logs out

  Examples:
  | tradeDetailsData         | tradeLocationData           | tradeDetailsData1        |
  | ownerDetailsTradeLicense | locationDetailsTradeLicense | tradeDetailsTradeLicense |

@Sanity @TradeLicense
Scenario Outline: Create new TL from CSCUSER-> collect fee -> forward to SI -> Change trade area and forward to CMOH
-> reject

  Given CSCUser logs in
  And user will select the required screen as "Create New License"
  And he enters trade owner details of new license <tradeDetailsData>
  And he enters trade location details of new license <tradeLocationData>
  And he enters trade details of new license <tradeDetailsData1>
  And he saves the application
  And he confirms to proceed
  And he copy trade application number
  And current user logs out

  When TL_PHS_JA logs in
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber
  And he chooses to act upon above application number

  And he forwards for TL approver TL_SI
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he changes trade area as "1200"
  And he forwards for TL approver TL_CMOH
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_CMOH logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he cancel the application
  And he confirms to proceed
  And he closes acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he verifies the application status
  And user will be notified by "Cancelled"
  And he verifies the License active
  And user will be notified by "No"
  And he closes search screen
  And current user logs out

  Examples:
  | tradeDetailsData         | tradeLocationData           | tradeDetailsData1        |
  | ownerDetailsTradeLicense | locationDetailsTradeLicense | tradeDetailsTradeLicense |

@Sanity @TradeLicense
Scenario Outline:Create new application by employee -> Rejected by commissioner

  Given TL_PHS_JA logs in
  And user will select the required screen as "Create New License"
  And he enters trade owner details of new license <tradeDetailsData>
  And he enters trade location details of new license <tradeLocationData>
  And he enters trade details of new license <tradeDetailsData1>
  And he saves the application
  And he confirms to proceed
  And he copy trade application number
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber
  And he chooses to act upon above application number

  And he forwards for TL approver TL_SI
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_Commissioner
  And he confirms to proceed
  And he closes acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_ADM_Commissioner logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he cancel the application
  And he confirms to proceed
  And he closes acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he verifies the application status
  And user will be notified by "Cancelled"
  And he verifies the License active
  And user will be notified by "No"
  And he closes search screen
  And current user logs out

  Examples:
  | tradeDetailsData         | tradeLocationData           | tradeDetailsData1        |
  | ownerDetailsTradeLicense | locationDetailsTradeLicense | tradeDetailsTradeLicense |

@Sanity @TradeLicense
Scenario Outline: Create New license from employee ->Change trade by si -> Rejected by commissioner

  Given TL_PHS_JA logs in
  And user will select the required screen as "Create New License"
  And he enters trade owner details of new license <tradeDetailsData>
  And he enters trade location details of new license <tradeLocationData>
  And he enters trade details of new license <tradeDetailsData1>
  And he saves the application
  And he confirms to proceed
  And he copy trade application number
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber
  And he chooses to act upon above application number

  And he forwards for TL approver TL_SI
  And he confirms to proceed
  And he closes acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he changes trade area as "1200"
  And he forwards for TL approver TL_Commissioner
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_ADM_Commissioner logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_JA logs in
  And he chooses to act upon above application number
  And he cancel the application
  And he confirms to proceed
  And he closes acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he verifies the application status
  And user will be notified by "Cancelled"
  And he verifies the License active
  And user will be notified by "No"
  And he closes search screen
  And current user logs out

  Examples:
  | tradeDetailsData         | tradeLocationData           | tradeDetailsData1        |
  | ownerDetailsTradeLicense | locationDetailsTradeLicense | tradeDetailsTradeLicense |

@Sanity @TradeLicense
Scenario Outline: Create new license from CSCUser ->forward to JA->Reassign to JA2->forward to SI->forward to commissioner->reject

  Given CSCUser logs in
  And user will select the required screen as "Create New License"
  And he enters trade owner details of new license <tradeDetailsData>
  And he enters trade location details of new license <tradeLocationData>
  And he enters trade details of new license <tradeDetailsData1>
  And he saves the application
  And he confirms to proceed
  And he copy trade application number
  And current user logs out

  When TL_PHS_JA logs in
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber
  And he chooses to act upon above application number
  And he choose to reassign the application
  And current user logs out

  When TL_PHS_JA2 logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SI
  And he confirms to proceed
  And he closes acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_Commissioner
  And he confirms to proceed
  And he closes acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_ADM_Commissioner logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he rejects the application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_JA2 logs in
  And he chooses to act upon above application number
  And he cancel the application
  And he confirms to proceed
  And he closes acknowledgement page
  And user will select the required screen as "Search Trade License"
  And he search existing application number
  And he verifies the application status
  And user will be notified by "Cancelled"
  And he verifies the License active
  And user will be notified by "No"
  And he closes search screen
  And current user logs out

  Examples:
  | tradeDetailsData         | tradeLocationData           | tradeDetailsData1        |
  | ownerDetailsTradeLicense | locationDetailsTradeLicense | tradeDetailsTradeLicense |









