Feature: Renewal of trade license

# License Renewal #
@Sanity @TradeLicense
Scenario Outline: Renewal of Trade License with legacy license (Approval by Commissioner)

  Given TL_PHS_JA logs in
  And user will select the required screen as "Create Legacy License"
  And he enters old license number
  And he enters trade owner details of new license <tradeDetailsData>
  And he enters trade location details of new license <tradeLocationData>
  And he enters trade details of new license <tradeDetailsData1>
  And he enters fee details of legacy trade license
  And he saves the application
  And he copies the license number and closes the acknowledgement
  And current user logs out

  When CSCUser logs in
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he choose action "Renew License"
  And he choose to renew trade license
  And he closes search screen
  And current user logs out

  When TL_PHS_JA logs in
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SI
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SS
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SS logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_Commissioner
  And he confirms to proceed
  And he closes acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_ADM_Commissioner logs in
  And he chooses to act upon above application number
  And he approves application
  And he confirms to proceed
  And he closes acknowledgement page
  And he verifies that application not in his inbox

  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Active"
  And he verifies the License active
  And user will be notified by "Yes"
  #And he generates the final license certificate
  And he closes search screen
  And current user logs out

  Examples:
  | tradeDetailsData         | tradeLocationData           | tradeDetailsData1        |
  | ownerDetailsTradeLicense | locationDetailsTradeLicense | tradeDetailsTradeLicense |

@Sanity @TradeLicense
Scenario Outline: Renewal of Trade License with legacy license and second level fee collection(Approval by Commissioner)

  Given TL_PHS_JA logs in
  And user will select the required screen as "Create Legacy License"
  And he enters old license number
  And he enters trade owner details of new license <tradeDetailsData>
  And he enters trade location details of new license <tradeLocationData>
  And he enters trade details of new license <tradeDetailsData1>
  And he enters fee details of legacy trade license
  And he saves the application
  And he copies the license number and closes the acknowledgement
  And current user logs out

  When CSCUser logs in
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he choose action "Renew License"
  And he choose to renew trade license
  And he closes search screen
  And current user logs out

  When TL_PHS_JA logs in
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SI
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he changes trade area as "1200"
  And he forwards for TL approver TL_SS
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SS logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_Commissioner
  And he confirms to proceed
  And he closes acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_ADM_Commissioner logs in
  And he chooses to act upon above application number
  And he approves application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_JA logs in
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber

  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Active"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And current user logs out

  Examples:
  | tradeDetailsData         | tradeLocationData           | tradeDetailsData1        |
  | ownerDetailsTradeLicense | locationDetailsTradeLicense | tradeDetailsTradeLicense |

@Sanity @TradeLicense
Scenario Outline: Renewal of Trade License with legacy license (Approval by AMOH)

  Given TL_PHS_JA logs in
  And user will select the required screen as "Create Legacy License"
  And he enters old license number
  And he enters trade owner details of new license <tradeDetailsData>
  And he enters trade location details of new license <tradeLocationData>
  And he enters trade details of new license <tradeDetailsData1>
  And he enters fee details of legacy trade license
  And he saves the application
  And he copies the license number and closes the acknowledgement
  And current user logs out

  When CSCUser logs in
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he choose action "Renew License"
  And he choose to renew trade license
  And he closes search screen
  And current user logs out

  When TL_PHS_JA logs in
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SI
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SS
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SS logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_AMOH
  And he confirms to proceed
  And he closes acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_AMOH logs in
  And he chooses to act upon above application number
  And he approves application
  And he confirms to proceed
  And he closes acknowledgement page
  And he verifies that application not in his inbox

  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Active"
  And he verifies the License active
  And user will be notified by "Yes"
  #And he generates the final license certificate
  And he closes search screen
  And current user logs out

  Examples:
  | tradeDetailsData         | tradeLocationData           | tradeDetailsData1        |
  | ownerDetailsTradeLicense | locationDetailsTradeLicense | tradeDetailsTradeLicense |

@Sanity @TradeLicense
Scenario Outline: Renewal of Trade License with legacy license and second level fee collection ( Approved by AMOH)

  Given TL_PHS_JA logs in
  And user will select the required screen as "Create Legacy License"
  And he enters old license number
  And he enters trade owner details of new license <tradeDetailsData>
  And he enters trade location details of new license <tradeLocationData>
  And he enters trade details of new license <tradeDetailsData1>
  And he enters fee details of legacy trade license
  And he saves the application
  And he copies the license number and closes the acknowledgement
  And current user logs out

  When CSCUser logs in
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he choose action "Renew License"
  And he choose to renew trade license
  And he closes search screen
  And current user logs out

  When TL_PHS_JA logs in
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SI
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he changes trade area as "1200"
  And he forwards for TL approver TL_SS
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SS logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_AMOH
  And he confirms to proceed
  And he closes acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_AMOH logs in
  And he chooses to act upon above application number
  And he approves application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_JA logs in
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber

  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Active"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And current user logs out

  Examples:
  | tradeDetailsData         | tradeLocationData           | tradeDetailsData1        |
  | ownerDetailsTradeLicense | locationDetailsTradeLicense | tradeDetailsTradeLicense |

@Sanity @TradeLicense
Scenario Outline: Renewal of Trade License with legacy license (Approval by MHO)

  Given TL_PHS_JA logs in
  And user will select the required screen as "Create Legacy License"
  And he enters old license number
  And he enters trade owner details of new license <tradeDetailsData>
  And he enters trade location details of new license <tradeLocationData>
  And he enters trade details of new license <tradeDetailsData1>
  And he enters fee details of legacy trade license
  And he saves the application
  And he copies the license number and closes the acknowledgement
  And current user logs out

  When CSCUser logs in
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he choose action "Renew License"
  And he choose to renew trade license
  And he closes search screen
  And current user logs out

  When TL_PHS_JA logs in
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SI
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SS
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SS logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_MHO
  And he confirms to proceed
  And he closes acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_MHO logs in
  And he chooses to act upon above application number
  And he approves application
  And he confirms to proceed
  And he closes acknowledgement page
  And he verifies that application not in his inbox

  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Active"
  And he verifies the License active
  And user will be notified by "Yes"
  #And he generates the final license certificate
  And he closes search screen
  And current user logs out

  Examples:
  | tradeDetailsData         | tradeLocationData           | tradeDetailsData1        |
  | ownerDetailsTradeLicense | locationDetailsTradeLicense | tradeDetailsTradeLicense |

  @Sanity @TradeLicense
  Scenario Outline: Renewal of Trade License with legacy license and second level fee collection ( Approved by MHO)

  Given TL_PHS_JA logs in
  And user will select the required screen as "Create Legacy License"
  And he enters old license number
  And he enters trade owner details of new license <tradeDetailsData>
  And he enters trade location details of new license <tradeLocationData>
  And he enters trade details of new license <tradeDetailsData1>
  And he enters fee details of legacy trade license
  And he saves the application
  And he copies the license number and closes the acknowledgement
  And current user logs out

  When CSCUser logs in
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he choose action "Renew License"
  And he choose to renew trade license
  And he closes search screen
  And current user logs out

  When TL_PHS_JA logs in
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SI
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he changes trade area as "1200"
  And he forwards for TL approver TL_SS
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SS logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_MHO
  And he confirms to proceed
  And he closes acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_MHO logs in
  And he chooses to act upon above application number
  And he approves application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_JA logs in
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber

  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Active"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And current user logs out

  Examples:
  | tradeDetailsData         | tradeLocationData           | tradeDetailsData1        |
  | ownerDetailsTradeLicense | locationDetailsTradeLicense | tradeDetailsTradeLicense |

@Sanity @TradeLicense
Scenario Outline: Renewal of Trade License with legacy license (Approval by CMOH)

  Given TL_PHS_JA logs in
  And user will select the required screen as "Create Legacy License"
  And he enters old license number
  And he enters trade owner details of new license <tradeDetailsData>
  And he enters trade location details of new license <tradeLocationData>
  And he enters trade details of new license <tradeDetailsData1>
  And he enters fee details of legacy trade license
  And he saves the application
  And he copies the license number and closes the acknowledgement
  And current user logs out

  When CSCUser logs in
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he choose action "Renew License"
  And he choose to renew trade license
  And he closes search screen
  And current user logs out

  When TL_PHS_JA logs in
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SI
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SS
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SS logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_CMOH
  And he confirms to proceed
  And he closes acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_CMOH logs in
  And he chooses to act upon above application number
  And he approves application
  And he confirms to proceed
  And he closes acknowledgement page
  And he verifies that application not in his inbox

  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Active"
  And he verifies the License active
  And user will be notified by "Yes"
  #And he generates the final license certificate
  And he closes search screen
  And current user logs out

  Examples:
  | tradeDetailsData         | tradeLocationData           | tradeDetailsData1        |
  | ownerDetailsTradeLicense | locationDetailsTradeLicense | tradeDetailsTradeLicense |

@Sanity @TradeLicense
Scenario Outline: Renewal of Trade License with legacy license and second level fee collection ( Approved by CMOH)

  Given TL_PHS_JA logs in
  And user will select the required screen as "Create Legacy License"
  And he enters old license number
  And he enters trade owner details of new license <tradeDetailsData>
  And he enters trade location details of new license <tradeLocationData>
  And he enters trade details of new license <tradeDetailsData1>
  And he enters fee details of legacy trade license
  And he saves the application
  And he copies the license number and closes the acknowledgement
  And current user logs out

  When CSCUser logs in
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he choose action "Renew License"
  And he choose to renew trade license
  And he closes search screen
  And current user logs out

  When TL_PHS_JA logs in
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SI
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he changes trade area as "1200"
  And he forwards for TL approver TL_SS
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SS logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_CMOH
  And he confirms to proceed
  And he closes acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_CMOH logs in
  And he chooses to act upon above application number
  And he approves application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_JA logs in
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber

  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Active"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And current user logs out

  Examples:
  | tradeDetailsData         | tradeLocationData           | tradeDetailsData1        |
  | ownerDetailsTradeLicense | locationDetailsTradeLicense | tradeDetailsTradeLicense |

@Sanity @TradeLicense
Scenario Outline: Renewal of Trade License with legacy license(Login with citizen)

  Given TL_PHS_JA logs in
  And user will select the required screen as "Create Legacy License"
  And he enters old license number
  And he enters trade owner details of new license <tradeDetailsData>
  And he enters trade location details of new license <tradeLocationData>
  And he enters trade details of new license <tradeDetailsData1>
  And he enters fee details of legacy trade license
  And he saves the application
  And he copies the license number and closes the acknowledgement
  And current user logs out

  When citizen logs in
  And citizen select required module as "Trade License"
  And citizen select required screen as "Renewal of License"
  And he choose to search with license number
  And he choose action "Renew License"
  And he choose to renew trade license
  And he closes search screen
  And current user logs out

  When TL_PHS_JA logs in
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SI
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_Commissioner
  And he confirms to proceed
  And he closes acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_ADM_Commissioner logs in
  And he chooses to act upon above application number
  And he approves application
  And he confirms to proceed
  And he closes acknowledgement page
  And he verifies that application not in his inbox

  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Active"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And current user logs out

  Examples:
  | tradeDetailsData         | tradeLocationData           | tradeDetailsData1        |
  | ownerDetailsTradeLicense | locationDetailsTradeLicense | tradeDetailsTradeLicense |

@Sanity @TradeLicense
Scenario Outline:  Renewal of Trade License with legacy license and second level fee collection(Login with citizen)

  Given TL_PHS_JA logs in
  And user will select the required screen as "Create Legacy License"
  And he enters old license number
  And he enters trade owner details of new license <tradeDetailsData>
  And he enters trade location details of new license <tradeLocationData>
  And he enters trade details of new license <tradeDetailsData1>
  And he enters fee details of legacy trade license
  And he saves the application
  And he copies the license number and closes the acknowledgement
  And current user logs out

  When citizen logs in
  And citizen select required module as "Trade License"
  And citizen select required screen as "Renewal of License"
  And he choose to search with license number
  And he choose action "Renew License"
  And he choose to renew trade license
  And he closes search screen
  And current user logs out

  When TL_PHS_JA logs in
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber
  And he chooses to act upon above application number
  And he forwards for TL approver TL_SI
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he changes trade area as "1200"
  And he forwards for TL approver TL_Commissioner
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_ADM_Commissioner logs in
  And he chooses to act upon above application number
  And he approves application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_JA logs in
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber

  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Active"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And current user logs out

  Examples:
  | tradeDetailsData         | tradeLocationData           | tradeDetailsData1        |
  | ownerDetailsTradeLicense | locationDetailsTradeLicense | tradeDetailsTradeLicense |

@Sanity @TradeLicense
Scenario Outline:Renew license from employee

  Given TL_PHS_JA logs in
  And user will select the required screen as "Create Legacy License"
  And he enters old license number
  And he enters trade owner details of new license <tradeDetailsData>
  And he enters trade location details of new license <tradeLocationData>
  And he enters trade details of new license <tradeDetailsData1>
  And he enters fee details of legacy trade license
  And he saves the application
  And he copies the license number and closes the acknowledgement

  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he choose action "Renew License"
  And he choose to renew trade license
  And he closes search screen

  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber
  And he chooses to act upon above application number

  And he forwards for TL approver TL_SI
  And he confirms to proceed
  And he closes acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he forwards for TL approver TL_Commissioner
  And he confirms to proceed
  And he closes acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_ADM_Commissioner logs in
  And he chooses to act upon above application number
  And he approves application
  And he confirms to proceed
  And he closes acknowledgement page
  And he verifies that application not in his inbox

  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Active"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And current user logs out

  Examples:
  | tradeDetailsData         | tradeLocationData           | tradeDetailsData1        |
  | ownerDetailsTradeLicense | locationDetailsTradeLicense | tradeDetailsTradeLicense |

@Sanity @TradeLicense
Scenario Outline:Renew license from employee with second level collection

  Given TL_PHS_JA logs in
  And user will select the required screen as "Create Legacy License"
  And he enters old license number
  And he enters trade owner details of new license <tradeDetailsData>
  And he enters trade location details of new license <tradeLocationData>
  And he enters trade details of new license <tradeDetailsData1>
  And he enters fee details of legacy trade license
  And he saves the application
  And he copies the license number and closes the acknowledgement

  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he choose action "Renew License"
  And he choose to renew trade license
  And he closes search screen

  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber
  And he chooses to act upon above application number

  And he forwards for TL approver TL_SI
  And he confirms to proceed
  And he closes acknowledgement page
  And he verifies that application not in his inbox
  And current user logs out

  When TL_PHS_SI logs in
  And he chooses to act upon above application number
  And he changes trade area as "1200"
  And he forwards for TL approver TL_Commissioner
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_ADM_Commissioner logs in
  And he chooses to act upon above application number
  And he approves application
  And he confirms to proceed
  And he closes acknowledgement page
  And current user logs out

  When TL_PHS_JA logs in
  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he choose action "Collect Fees"
  And he choose to payTax of applicationNumber

  And user will select the required screen as "Search Trade License"
  And he choose to search with license number
  And he verifies the application status
  And user will be notified by "Active"
  And he verifies the License active
  And user will be notified by "Yes"
  And he closes search screen
  And current user logs out

  Examples:
  | tradeDetailsData         | tradeLocationData           | tradeDetailsData1        |
  | ownerDetailsTradeLicense | locationDetailsTradeLicense | tradeDetailsTradeLicense |


