Feature: Register Complaint

As a citizen register complaint directly in website

Background: Mapping employee to route the complaint

  Given admin logs in
  And user will select the required screen as "Define Bulk Router"
  And user will create bulk router with boundary wardBoundary
  And current user logs out

@Sanity @Grievance
Scenario Outline: Register a Complaint with Citizen Login

  Given citizen logs in
  And citizen select required module as "Grievance Redressal"
  And citizen select required screen as "Register Grievance"
  And he choose to enter grievance details as <grievanceDetails>
  And user will be notified by "successfully"
  And he copies CRN and closes the acknowledgement
#  And he choose to search for above complaint
#  And he selects the user for which the above complaint has routed
  And citizen sign out

  And TL_PHS_SS logs in
  And he chooses to act upon above application number
  And he mark status as COMPLETED
  And user will be notified by "successfully"
  And he verifies that application not in his inbox
  And current user logs out

  Examples:
  | grievanceDetails |
  | grievanceDetails |

@Sanity @Grievance
Scenario Outline:  Official Register Grievance

  Given TL_PHS_SI logs in
  And user will select the required screen as "Register Grievance"
  And he choose to enter contact information as <contactDetails>
  And he choose to enter grievance details as <grievanceDetails>
  And user will be notified by "successfully"
  And he copies CRN and closes the acknowledgement
  And current user logs out

  Examples:
  | contactDetails | grievanceDetails |
  | contactInfo    | grievanceDetails |

@Sanity @Grievance
Scenario Outline: Official Register Grievance and forwards

  Given TL_PHS_SI logs in
  And user will select the required screen as "Register Grievance"
  And he choose to enter contact information as <contactDetails>
  And he choose to enter grievance details as <grievanceDetails>
  And user will be notified by "successfully"
  And he copies CRN and closes the acknowledgement
  #And user will select the required screen as "Search Grievance"
  #And he selects the user for which the above complaint has routed
  And current user logs out

  And TL_PHS_SS logs in
  And he chooses to act upon above application number
  And he forwards for approver Grievanceofficer1
  And he verifies that application not in his inbox
  And current user logs out

  When TL_ADM_Commissioner logs in
  And he chooses to act upon above application number
  And he mark status as COMPLETED
  And user will be notified by "successfully"
  And he verifies that application not in his inbox
  And current user logs out

  Examples:
  | contactDetails | grievanceDetails |
  | contactInfo    | grievanceDetails |

@Sanity @Grievance
Scenario Outline: Citizen register a complaint and official forwards it to next level

  Given citizen logs in
  # When he choose to register complaint with his login
  And citizen select required module as "Grievance Redressal"
  And citizen select required screen as "Register Grievance"
  And he choose to enter grievance details as <grievanceDetails>
  And user will be notified by "successfully"
  And he copies CRN and closes the acknowledgement
  # And he choose to search for above complaint
  # And he selects the user for which the above complaint has routed
  And citizen sign out

  And TL_PHS_SS logs in
  And he chooses to act upon above application number
  And he forwards for approver Grievanceofficer1
  And he verifies that application not in his inbox
  And current user logs out

  When TL_ADM_Commissioner logs in
  And he chooses to act upon above application number
  And he mark status as COMPLETED
  And user will be notified by "successfully"
  And he verifies that application not in his inbox
  And current user logs out

  Examples:
  | grievanceDetails |
  | grievanceDetails |

@Sanity @Grievance @New5
Scenario Outline: Citizen register a complaint and withdraw it

  Given citizen logs in
  #When he choose to register complaint with his login
  And citizen select required module as "Grievance Redressal"
  And citizen select required screen as "Register Grievance"
  And he choose to enter grievance details as <grievanceDetails>
  And user will be notified by "successfully"
  And he copies CRN and closes the acknowledgement
  # And he choose to search for above complaint
  # nd he selects the user for which the above complaint has routed
  And he search complaint in his Inbox
  And he WITHDRAWN the complaint
  And citizen sign out

  When TL_PHS_SS logs in
  And he verifies that application not in his inbox
  And current user logs out

  Examples:
  | grievanceDetails |
  | grievanceDetails |

@Sanity @Grievance
Scenario Outline: Citizen register a complaint, officer reject it and citizen reopens the complaint

  Given citizen logs in
  # When he choose to register complaint with his login
  And citizen select required module as "Grievance Redressal"
  And citizen select required screen as "Register Grievance"
  And he choose to enter grievance details as <grievanceDetails>
  And user will be notified by "successfully"
  And he copies CRN and closes the acknowledgement
  # And he choose to search for above complaint
  # And he selects the user for which the above complaint has routed
  And citizen sign out

  And TL_PHS_SS logs in
  And he chooses to act upon above application number
  And he mark status as REJECTED
  And user will be notified by "successfully"
  And he verifies that application not in his inbox
  And current user logs out

  When citizen logs in
  And he search complaint in his Inbox
  And he REOPENED the complaint
  And citizen sign out

  Examples:
  | grievanceDetails |
  | grievanceDetails |

@Grievance
Scenario Outline: Create complaint through citizen and give the feedback ratings

  Given citizen logs in
  And citizen select required module as "Grievance Redressal"
  And citizen select required screen as "Register Grievance"
  And he choose to enter grievance details as <grievanceDetails>
  And user will be notified by "successfully"
  And he copies CRN and closes the acknowledgement
  And citizen sign out

  And TL_PHS_SS logs in
  And he chooses to act upon above application number
  And he mark status as COMPLETED
  And user will be notified by "successfully"
  And he verifies that application not in his inbox
  And current user logs out

  Given citizen logs in
  And he search the completed complaints from in his inbox
  And user give the feedback ratings
  And citizen sign out

  And TL_PHS_SS logs in
  And user will select the required screen as "Search Grievance"
  And he search complaint with crn
  And current user logs out

  Examples:
    | grievanceDetails |
    | grievanceDetails |

@Sanity @Grievance
Scenario Outline: Citizen register a complaint, officer completed it and citizen reopens the complaint

  Given citizen logs in
  And citizen select required module as "Grievance Redressal"
  And citizen select required screen as "Register Grievance"
  And he choose to enter grievance details as <grievanceDetails>
  And user will be notified by "successfully"
  And he copies CRN and closes the acknowledgement
  And citizen sign out

  And TL_PHS_SS logs in
  And he chooses to act upon above application number
  And he mark status as COMPLETED
  And user will be notified by "successfully"
  And he verifies that application not in his inbox
  And current user logs out

  Examples:
    | grievanceDetails |
    | grievanceDetails |


